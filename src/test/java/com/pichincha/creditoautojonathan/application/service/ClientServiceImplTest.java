package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.output.port.ClientRepositoryPort;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static com.pichincha.creditoautojonathan.commons.util.ConvertCsvModel.csvToModelClient;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ClientServiceImplTest {

    @InjectMocks
    private PropertiesFilesCsv propertiesFilesCsv;

    @InjectMocks
    private ClientServiceImpl clientService;

    @Mock
    private ClientRepositoryPort clientRepositoryPort;

    private List<ClientModel> clientModels;

    @BeforeEach
    void setUp() {
        clientModels = getAllClientMock();
    }
    private List<ClientModel> getAllClientMock() {
        List<ClientModel> clientModelList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(propertiesFilesCsv.getClient())))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                ClientModel ClientModel = csvToModelClient(values);
                clientModels.add(ClientModel);
            }
        } catch (Exception e) {
        }
        return clientModelList;
    }

    private ClientModel getClientMock() {
        return ClientModel.builder()
                .id(1L)
                .identification("120345640")
                .name("Cliente")
                .address("Avenida Libertadores")
                .phone("3212332309")
                .build();
    }

    @Test
    @DisplayName("Listar todos los clientes")
    void testGetAllClient() {
        given(clientRepositoryPort.getAll()).willReturn(clientModels);
        assertNotNull(clientService.getAll());
    }

    @Test
    @DisplayName("Obtener el Cliente")
    void testGetClient() {
        given(clientRepositoryPort.get(1)).willReturn(getClientMock());
        assertNotNull(clientService.get(1));
    }

    @Test
    @DisplayName("Crear el Cliente")
    void testCreateClient() {
        given(clientRepositoryPort.create(any(ClientModel.class))).willReturn(getClientMock());
        var clientModel = clientService.create(getClientMock());

        verify(clientRepositoryPort, times(1)).create(any(ClientModel.class));
        Assertions.assertNotNull(clientModel.getId());
    }

    @Test
    @DisplayName("Actualizar el Cliente")
    void testUpdateClient() {
        given(clientRepositoryPort.get(1)).willReturn(getClientMock());
        given(clientRepositoryPort.update(any(ClientModel.class))).willReturn(getClientMock());
        var clientModel = clientService.update(getClientMock());

        Assertions.assertNotNull(clientModel.getId());
    }
}
