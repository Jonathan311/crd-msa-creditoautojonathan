package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.output.port.ExecutiveRepositoryPort;
import com.pichincha.creditoautojonathan.application.output.port.YardRepositoryPort;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static com.pichincha.creditoautojonathan.commons.util.ConvertCsvModel.csvToModelExecutive;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ExecutiveServiceImplTest {

    @InjectMocks
    private PropertiesFilesCsv propertiesFilesCsv;

    @InjectMocks
    private ExecutiveServiceImpl executiveService;

    @Mock
    private ExecutiveRepositoryPort executiveRepositoryPort;
    @Mock
    private YardRepositoryPort yardRepositoryPort;

    private List<ExecutiveModel> executiveModels;

    private YardModel yardModel;

    @BeforeEach
    void setUp() {
        executiveModels = getAllExecutiveMock();
        yardModel = getYardMock();
    }

    private YardModel getYardMock() {
        return YardModel.builder()
                .id(1L)
                .name("Patio Chevrolet")
                .address("Avenida Libertadores")
                .phone("3212332309")
                .pointSaleNumber(5)
                .build();
    }

    private ExecutiveModel getExecutiveMock() {
        return ExecutiveModel.builder()
                .id(1L)
                .identification("1212405")
                .name("Patio Chevrolet")
                .address("Avenida Libertadores")
                .yard(getYardMock())
                .build();
    }
    private List<ExecutiveModel> getAllExecutiveMock() {
        List<ExecutiveModel> executiveModelList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(propertiesFilesCsv.getExecutive())))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                ExecutiveModel executiveModel = csvToModelExecutive(values);
                executiveModel.setYard(yardModel);

                executiveModels.add(executiveModel);
            }
        } catch (Exception e) {
        }
        return executiveModelList;
    }

    @Test
    @DisplayName("Listar todos los ejecutivos")
    void testGetAllExecutive() {
        given(executiveRepositoryPort.getAll()).willReturn(executiveModels);
        assertNotNull(executiveService.getAll());
    }

    @Test
    @DisplayName("Obtener el ejecutivo")
    void testGetExecutive() {
        given(executiveRepositoryPort.get(1)).willReturn(getExecutiveMock());
        assertNotNull(executiveService.get(1));
    }

    @Test
    @DisplayName("Crear el ejecutivo")
    void testCreateExecutive() {
        given(yardRepositoryPort.get(1)).willReturn(getYardMock());
        given(executiveRepositoryPort.create(any(ExecutiveModel.class))).willReturn(getExecutiveMock());
        var executiveModel = executiveService.create(getExecutiveMock());

        verify(executiveRepositoryPort, times(1)).create(any(ExecutiveModel.class));
        Assertions.assertNotNull(executiveModel.getId());
    }

    @Test
    @DisplayName("Actualizar el ejecutivo")
    void testUpdateExecutive() {
        given(yardRepositoryPort.get(1)).willReturn(getYardMock());
        given(executiveRepositoryPort.get(1)).willReturn(getExecutiveMock());
        given(executiveRepositoryPort.update(any(ExecutiveModel.class))).willReturn(getExecutiveMock());
        var executiveModel = executiveService.update(getExecutiveMock());

        Assertions.assertNotNull(executiveModel.getId());
    }
}
