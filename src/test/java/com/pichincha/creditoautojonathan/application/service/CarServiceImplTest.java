package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.output.port.CarRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import com.pichincha.creditoautojonathan.domain.model.CarModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class CarServiceImplTest {

    @InjectMocks
    private CarServiceImpl carService;

    @Mock
    private CarRepositoryPort carRepositoryPort;

    private List<CarModel> carModels;

    private BrandModel brandModel;

    @BeforeEach
    void setUp() {
        carModels = getAllCarMock();
        brandModel = getBrandMock();
    }

    private BrandModel getBrandMock() {
        return BrandModel.builder()
                .id(1L)
                .name("Chevrolet")
                .build();
    }

    private CarModel getCarMock() {
        return CarModel.builder()
                .id(1L)
                .plate("SSS111")
                .model("2023")
                .numberChassis("1321456454654651")
                .type("Toyota")
                .cylindrical("1.6")
                .appraisal("150000000")
                .status("1")
                .brand(brandModel)
                .build();
    }

    private List<CarModel> getAllCarMock() {
        List<CarModel> carModelList = new ArrayList<>();
        carModelList.add(getCarMock());
        return carModelList;
    }

    @Test
    @DisplayName("Listar todos los vehiculos")
    void testGetAllCar() {
        given(carRepositoryPort.getAll()).willReturn(carModels);
        assertNotNull(carService.getAll());
    }

    @Test
    @DisplayName("Obtener el vehiculo")
    void testGetCar() {
        given(carRepositoryPort.get(1)).willReturn(getCarMock());
        assertNotNull(carService.get(1));
    }
}
