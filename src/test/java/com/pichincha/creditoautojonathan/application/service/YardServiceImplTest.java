package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.output.port.YardRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class YardServiceImplTest {

    @InjectMocks
    private YardServiceImpl yardService;

    @Mock
    private YardRepositoryPort yardRepositoryPort;

    private List<YardModel> yardModels;

    @BeforeEach
    void setUp() {
        yardModels = getAllYardMock();
    }
    private List<YardModel> getAllYardMock() {
        List<YardModel> yardModelList = new ArrayList<>();
        yardModelList.add(getYardMock());
        return yardModelList;
    }
    private YardModel getYardMock() {
        return YardModel.builder()
                .id(1L)
                .name("Patio Chevrolet")
                .address("Avenida Libertadores")
                .phone("3212332309")
                .pointSaleNumber(5)
                .build();
    }

    @Test
    @DisplayName("Listar todos los patios")
    void testGetAllYard() {
        given(yardRepositoryPort.getAll()).willReturn(yardModels);
        assertNotNull(yardService.getAll());
    }

    @Test
    @DisplayName("Obtener el patio")
    void testGetYard() {
        given(yardRepositoryPort.get(1)).willReturn(getYardMock());
        assertNotNull(yardService.get(1));
    }

    @Test
    @DisplayName("Crear el patio")
    void testCreateYard() {
        given(yardRepositoryPort.create(any(YardModel.class))).willReturn(getYardMock());
        var yard = yardService.create(getYardMock());

        verify(yardRepositoryPort, times(1)).create(any(YardModel.class));
        Assertions.assertNotNull(yard.getId());
    }

    @Test
    @DisplayName("Actualizar el patio")
    void testUpdateYard() {
        given(yardRepositoryPort.get(1)).willReturn(getYardMock());
        given(yardRepositoryPort.update(any(YardModel.class))).willReturn(getYardMock());
        var yard = yardService.update(getYardMock());

        Assertions.assertNotNull(yard.getId());
    }
}
