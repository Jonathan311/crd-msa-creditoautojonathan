package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.output.port.BrandRepositoryPort;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class BrandServiceImplTest {

    @InjectMocks
    private PropertiesFilesCsv propertiesFilesCsv;

    @InjectMocks
    private BrandServiceImpl brandService;

    @Mock
    private BrandRepositoryPort brandRepositoryPort;

    private List<BrandModel> brandModels;

    @BeforeEach
    void setUp() {
        brandModels = getBrandMock();
    }

    private List<BrandModel> getBrandMock() {
        List<BrandModel> brandModels = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(propertiesFilesCsv.getBrand())))) {
            String line;
            while ((line = br.readLine()) != null) {
                BrandModel brandModel = BrandModel.builder().name(line).build();
                brandModels.add(brandModel);
            }
        } catch (Exception e) {
        }
        return brandModels;
    }

    @Test
    @DisplayName("Listar todas las marcas")
    void testGetAllBrand() {
        given(brandRepositoryPort.getAll()).willReturn(brandModels);
        assertNotNull(brandService.getAll());
    }
}
