package com.pichincha.creditoautojonathan;

import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.MappingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Arquitectura tradisional para el recurso Crd-msa-creditoautojonathan-dev extendiendo
 * de la interfaz generada.
 * <br/>
 * <b>Class</b>: OptimusMainApplication<br/>
 * <b>Copyright</b>: &copy; 2023 Banco Pichincha<br/>
 * <b>Company</b>: Everis del Per&uacute;. <br/>
 *
 * @author Banco Pichincha <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 *
 * <li>Jonathan Julio</li>
 *
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 *
 * <li>Jul 5, 2023 Creaci&oacute;n de Clase.</li>
 *
 * </ul>
 * @version 1.0
 */
@Slf4j
@EnableConfigurationProperties({
        PropertiesFilesCsv.class,
        GlobalProperties.class,
        MappingResponse.class
})
@SpringBootApplication
public class OptimusMainApplication implements ApplicationListener<ContextRefreshedEvent> {
    private final GlobalProperties globalProperties;

    public OptimusMainApplication(GlobalProperties pGlobalProperties) {
        this.globalProperties = pGlobalProperties;
    }


    public static void main(String[] args) {
        SpringApplication.run(OptimusMainApplication.class, args);
    }
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.info(Constants.LONG_LINE);
        log.info(Constants.LOG_START_PROJECT, globalProperties.getName());
        log.info(Constants.LOG_PORT_OF_PROJECT, globalProperties.getRestPort());
        log.info(Constants.LOG_PROJECT_VERSION, globalProperties.getVersion());
        log.info(Constants.LOG_RUN_OK);
        log.info(Constants.LONG_LINE);
    }
}