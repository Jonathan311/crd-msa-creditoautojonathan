package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.executive;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Slf4j
@Getter
@Setter
@Tag(name = "ExecutiveCreateRequest", description = "Solicitud de crear un ejecutivo")
public class ExecutiveCreateRequest extends SelfValidating<ExecutiveCreateRequest> {

    @Schema(description = "Identificación del ejecutivo", required = true)
    @NotEmpty(message = "{identification.empty}")
    @NotNull(message = "{identification.null}")
    @Size(min = 1, max = 50, message = "{identification.size}")
    private String identification;

    @Schema(description = "Nombre del ejecutivo", required = true)
    @NotEmpty(message = "{name.empty}")
    @NotNull(message = "{name.null}")
    @Size(min = 1, max = 100, message = "{name.size}")
    private String name;

    @Schema(description = "Apellido del ejecutivo", required = true)
    @NotEmpty(message = "{surname.empty}")
    @NotNull(message = "{surname.null}")
    @Size(min = 1, max = 100, message = "{surname.size}")
    private String surname;

    @Schema(description = "Direccion del ejecutivo", required = true)
    @NotEmpty(message = "{address.empty}")
    @NotNull(message = "{address.null}")
    @Size(min = 1, max = 100, message = "{address.size}")
    private String address;

    @Schema(description = "Teléfono convencional", required = true)
    @NotEmpty(message = "{conventionalTelephone.empty}")
    @NotNull(message = "{conventionalTelephone.null}")
    @Size(min = 1, max = 50, message = "{conventionalTelephone.size}")
    private String conventionalTelephone;

    @Schema(description = "Celular del ejecutivo", required = true)
    @NotEmpty(message = "{cellPhone.empty}")
    @NotNull(message = "{cellPhone.null}")
    @Size(min = 1, max = 50, message = "{cellPhone.size}")
    private String cellPhone;

    @Schema(description = "Id del patio", required = true)
    @NotNull(message = "{yardId.null}")
    private Long yardId;

    @Schema(description = "Edad del ejecutivo", required = true)
    @NotNull(message = "{age.null}")
    private Integer age;

    public ExecutiveCreateRequest(String identification, String name, String surname, String address, String conventionalTelephone, String cellPhone, Long yardId, Integer age) {
        this.identification = identification;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.conventionalTelephone = conventionalTelephone;
        this.cellPhone = cellPhone;
        this.yardId = yardId;
        this.age = age;
        this.validateSelf();
    }
}