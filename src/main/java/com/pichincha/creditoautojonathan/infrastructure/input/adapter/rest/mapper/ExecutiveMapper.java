package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper;

import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.executive.ExecutiveCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.executive.ExecutiveUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ExecutiveMapper {
    @Mapping(target="yard.id", source="yardId")
    ExecutiveModel toDomain(ExecutiveCreateRequest source);
    @Mapping(target="yard.id", source="yardId")
    ExecutiveModel toDomain(ExecutiveUpdateRequest source);
}
