package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;

@Slf4j
@Getter
@Setter
@Tag(name = "CreditRequestCreateRequest", description = "Solicitud de crear un crédito")
public class CreditRequestCreateRequest extends SelfValidating<CreditRequestCreateRequest> {

    @Schema(description = "Fecha de elaboración", required = true)
    @NotNull(message = "{dateElaboration.null}")
    private LocalDate dateElaboration;

    @Schema(description = "Id del cliente", required = true)
    @NotNull(message = "{clientId.null}")
    private Integer clientId;

    @Schema(description = "Id del Patio", required = true)
    @NotNull(message = "{yardId.null}")
    private Integer yardId;

    @Schema(description = "Id del ejecutivo", required = true)
    @NotNull(message = "{executiveId.null}")
    private Integer executiveId;

    @Schema(description = "Id del vehiculo", required = true)
    @NotNull(message = "{carId.null}")
    private Integer carId;

    @Schema(description = "Meses de plazo", required = true)
    @NotNull(message = "{monthsTerm.null}")
    private Integer monthsTerm;

    @Schema(description = "Cuotas", required = true)
    @NotNull(message = "{dues.null}")
    private Integer dues;

    @Schema(description = "Entrada", required = true)
    @NotNull(message = "{entry.null}")
    private BigDecimal entry;

    @Schema(description = "Estado del crédito", required = true)
    @Pattern(regexp = "R", message = "{status.mask}")
    @NotEmpty(message = "{status.empty}")
    @NotNull(message = "{status.null}")
    private String status;

    @Schema(description = "Observación")
    @Size(max = 255, message = "{observation.size}")
    private String observation;

    public CreditRequestCreateRequest(LocalDate dateElaboration, Integer clientId, Integer yardId, Integer executiveId,
                                      Integer carId, Integer monthsTerm, Integer dues, BigDecimal entry, String status,
                                      String observation) {
        this.dateElaboration = dateElaboration;
        this.clientId = clientId;
        this.yardId = yardId;
        this.executiveId = executiveId;
        this.carId = carId;
        this.monthsTerm = monthsTerm;
        this.dues = dues;
        this.entry = entry;
        this.status = status;
        this.observation = observation;
        this.validateSelf();
    }
}