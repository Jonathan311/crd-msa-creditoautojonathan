package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper;

import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.clientyard.ClientYardCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.clientyard.ClientYardUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClientYardMapper {
    @Mapping(target="client.id", source="clientId")
    @Mapping(target="yard.id", source="yardId")
    ClientYardModel toDomain(ClientYardCreateRequest source);
    @Mapping(target="client.id", source="clientId")
    @Mapping(target="yard.id", source="yardId")
    ClientYardModel toDomain(ClientYardUpdateRequest source);
}
