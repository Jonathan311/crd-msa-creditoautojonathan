package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.clientyard;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Slf4j
@Getter
@Setter
@Tag(name = "ClientYardCreateRequest", description = "Solicitud de crear un cliente por patio")
public class ClientYardCreateRequest extends SelfValidating<ClientYardCreateRequest> {

    @Schema(description = "Fecha de asignación", required = true)
    @NotNull(message = "{dateAssignment.null}")
    private Date dateAssignment;

    @Schema(description = "Id del cliente", required = true)
    @NotNull(message = "{clientId.null}")
    private Long clientId;

    @Schema(description = "Id del patio", required = true)
    @NotNull(message = "{yardId.null}")
    private Long yardId;

    public ClientYardCreateRequest(Date dateAssignment, Long clientId, Long yardId) {
        this.dateAssignment = dateAssignment;
        this.clientId = clientId;
        this.yardId = yardId;
        this.validateSelf();
    }
}