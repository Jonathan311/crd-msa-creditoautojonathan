package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper;

import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.client.ClientCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.client.ClientUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClientMapper {
    ClientModel toDomain(ClientCreateRequest source);
    ClientModel toDomain(ClientUpdateRequest source);
}
