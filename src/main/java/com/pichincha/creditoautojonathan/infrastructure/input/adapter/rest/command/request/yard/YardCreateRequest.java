package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.yard;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Slf4j
@Getter
@Setter
@Tag(name = "YardCreateRequest", description = "Solicitud de crear un patio")
public class YardCreateRequest extends SelfValidating<YardCreateRequest> {

    @Schema(description = "Nombre del patio", required = true)
    @NotEmpty(message = "{name.empty}")
    @NotNull(message = "{name.null}")
    @Size(min = 1, max = 100, message = "{name.size}")
    private String name;

    @Schema(description = "Dirección del patio", required = true)
    @NotEmpty(message = "{address.empty}")
    @NotNull(message = "{address.null}")
    @Size(min = 1, max = 100, message = "{address.size}")
    private String address;

    @Schema(description = "Teléfono del patio", required = true)
    @NotEmpty(message = "{phone.empty}")
    @NotNull(message = "{phone.null}")
    @Size(min = 1, max = 20, message = "{phone.size}")
    private String phone;

    @Schema(description = "Número de punto de venta", required = true)
    @NotNull(message = "{pointSaleNumber.null}")
    private Integer pointSaleNumber;

    public YardCreateRequest(String name, String address, String phone, Integer pointSaleNumber) {
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.pointSaleNumber = pointSaleNumber;
        this.validateSelf();
    }
}