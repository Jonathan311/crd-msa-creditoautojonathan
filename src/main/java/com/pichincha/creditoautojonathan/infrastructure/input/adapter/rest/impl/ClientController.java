package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.impl;

import com.pichincha.creditoautojonathan.application.input.port.ClientService;
import com.pichincha.creditoautojonathan.commons.CustomResponse;
import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.logs.LogsHelper;
import com.pichincha.creditoautojonathan.commons.logs.enums.OperationType;
import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.client.ClientCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.client.ClientUpdateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper.ClientMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.CLIENT_CONTROLLER_PATH)
@Tag(name = "Cliente Controller", description = "Controlador de metodos para la gestion de cliente")
public class ClientController {

    private final ClientService clientService;

    private final ClientMapper clientMapper;

    private final GlobalProperties globalProperties;

    private final CustomResponse customResponse;

    @PostConstruct
    public void loadDataClient() {
        clientService.loadData();
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = ClientModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getAllClient", description = "Obtiene info de los clientes")
    @GetMapping(value = Constants.CLIENT_CONTROLLER_GET_ALL_PATH)
    public ResponseEntity<CustomResponse> getAllClient() {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENT_GET_ALL, globalProperties);

        final List<ClientModel> brandModels = clientService.getAll();
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(brandModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_GET_ALL, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ClientModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getClient", description = "Obtiene info del cliente")
    @GetMapping(value = Constants.CLIENT_CONTROLLER_GET_PATH)
    public ResponseEntity<CustomResponse> getClient(@Valid @PathVariable("clientId") Integer clientId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENT_GET, globalProperties);

        final ClientModel data = clientService.get(clientId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(data);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_GET, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ClientModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "createClient", description = "Registra un nuevo cliente")
    @PostMapping(value = Constants.CLIENT_CONTROLLER_POST_PATH)
    public ResponseEntity<CustomResponse> createClient(@RequestBody ClientCreateRequest clientCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(clientCreateRequest, OperationType.CLIENT_POST_REQUEST, globalProperties);

        ClientModel domain = clientMapper.toDomain(clientCreateRequest);
        domain = clientService.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_POST_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ClientModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "updateClient", description = "Actualiza un cliente")
    @PutMapping(value = Constants.CLIENT_CONTROLLER_PUT_PATH)
    public ResponseEntity<CustomResponse> updateClient(@RequestBody ClientUpdateRequest clientUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(clientUpdateRequest, OperationType.CLIENT_PUT_REQUEST, globalProperties);

        ClientModel domain = clientMapper.toDomain(clientUpdateRequest);
        domain = clientService.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "deleteClient", description = "Elimina un cliente")
    @DeleteMapping(value = Constants.CLIENT_CONTROLLER_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteClient(@Valid @PathVariable("clientId") Integer clientId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENT_DELETE_REQUEST, globalProperties);

        clientService.delete(clientId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_DELETE_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(responseSuccess);
    }
}
