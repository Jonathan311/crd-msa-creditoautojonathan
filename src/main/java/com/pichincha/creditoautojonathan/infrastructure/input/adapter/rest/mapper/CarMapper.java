package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper;

import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.car.CarCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.car.CarUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CarMapper {
    @Mapping(target="brand.id", source="brandId")
    CarModel toDomain(CarCreateRequest source);
    @Mapping(target="brand.id", source="brandId")
    CarModel toDomain(CarUpdateRequest source);
}
