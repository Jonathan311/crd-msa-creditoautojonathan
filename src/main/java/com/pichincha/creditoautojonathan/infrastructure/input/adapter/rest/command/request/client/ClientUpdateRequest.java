package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.client;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Slf4j
@Getter
@Setter
@Tag(name = "ClientUpdateRequest", description = "Solicitud de actualizar un cliente")
public class ClientUpdateRequest extends SelfValidating<ClientUpdateRequest> {

    @Schema(description = "ID del cliente", required = true)
    @NotNull(message = "{id.null}")
    private Long id;

    @Schema(description = "Nombre del cliente", required = true)
    @NotEmpty(message = "{identification.empty}")
    @NotNull(message = "{identification.null}")
    @Size(min = 1, max = 50, message = "{identification.size}")
    private String identification;

    @Schema(description = "Nombre del cliente", required = true)
    @NotEmpty(message = "{name.empty}")
    @NotNull(message = "{name.null}")
    @Size(min = 1, max = 100, message = "{name.size}")
    private String name;

    @Schema(description = "Edad del cliente", required = true)
    @NotNull(message = "{age.null}")
    private Integer age;

    @Schema(description = "Fecha de nacimiento del cliente", required = true)
    @NotNull(message = "{birthdate.null}")
    private Date birthdate;

    @Schema(description = "Apellido del cliente", required = true)
    @NotEmpty(message = "{surname.empty}")
    @NotNull(message = "{surname.null}")
    @Size(min = 1, max = 100, message = "{surname.size}")
    private String surname;

    @Schema(description = "Dirección del cliente", required = true)
    @NotEmpty(message = "{address.empty}")
    @NotNull(message = "{address.null}")
    @Size(min = 1, max = 100, message = "{address.size}")
    private String address;

    @Schema(description = "Teléfono del cliente", required = true)
    @NotEmpty(message = "{phone.empty}")
    @NotNull(message = "{phone.null}")
    @Size(min = 1, max = 50, message = "{phone.size}")
    private String phone;

    @Schema(description = "Estado civil del cliente", required = true)
    @NotEmpty(message = "{civilStatus.empty}")
    @NotNull(message = "{civilStatus.null}")
    @Size(min = 1, max = 10, message = "{civilStatus.size}")
    private String civilStatus;

    @Schema(description = "Identificación del conyugue", required = true)
    @NotEmpty(message = "{spouseIdentification.empty}")
    @NotNull(message = "{spouseIdentification.null}")
    @Size(min = 1, max = 50, message = "{spouseIdentification.size}")
    private String spouseIdentification;

    @Schema(description = "Nombre del conyugue", required = true)
    @NotEmpty(message = "{spouseName.empty}")
    @NotNull(message = "{spouseName.null}")
    @Size(min = 1, max = 100, message = "{spouseName.size}")
    private String spouseName;

    @Schema(description = "Sujeto de crédito", required = true)
    @NotNull(message = "{creditSubject.null}")
    private Boolean creditSubject;

    public ClientUpdateRequest(Long id, String identification, String name, Integer age, Date birthdate, String surname, String address, String phone, String civilStatus, String spouseIdentification, String spouseName, Boolean creditSubject) {
        this.id = id;
        this.identification = identification;
        this.name = name;
        this.age = age;
        this.birthdate = birthdate;
        this.surname = surname;
        this.address = address;
        this.phone = phone;
        this.civilStatus = civilStatus;
        this.spouseIdentification = spouseIdentification;
        this.spouseName = spouseName;
        this.creditSubject = creditSubject;
        this.validateSelf();
    }
}
