package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.impl;

import com.pichincha.creditoautojonathan.application.input.port.BrandService;
import com.pichincha.creditoautojonathan.commons.CustomResponse;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.logs.LogsHelper;
import com.pichincha.creditoautojonathan.commons.logs.enums.OperationType;
import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET})
@RequestMapping(Constants.BRAND_CONTROLLER_PATH)
@Tag(name = "Marca Controller", description = "Controlador de metodos para la gestion de marcas")
public class BrandController {

    private final BrandService brandService;

    private final GlobalProperties globalProperties;

    private final CustomResponse customResponse;

    @PostConstruct
    public void loadDataBrand() {
        brandService.loadData();
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = BrandModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "getAllBrand", description = "Obtiene info de las marcas")
    @GetMapping(value = Constants.BRAND_CONTROLLER_GET_ALL_PATH)
    public ResponseEntity<CustomResponse> getAllBrand() {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.BRAND_GET_ALL, globalProperties);

        final List<BrandModel> brandModels = brandService.getAll();
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(brandModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.BRAND_GET_ALL, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
}
