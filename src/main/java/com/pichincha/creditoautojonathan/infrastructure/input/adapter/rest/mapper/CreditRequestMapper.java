package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper;

import com.pichincha.creditoautojonathan.domain.model.CreditRequestModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest.CreditRequestCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest.CreditRequestPatchRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest.CreditRequestUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CreditRequestMapper {
    @Mapping(target="client.id", source="clientId")
    @Mapping(target="yard.id", source="yardId")
    @Mapping(target="executive.id", source="executiveId")
    @Mapping(target="car.id", source="carId")
    CreditRequestModel toDomain(CreditRequestCreateRequest source);
    @Mapping(target="client.id", source="clientId")
    @Mapping(target="yard.id", source="yardId")
    @Mapping(target="executive.id", source="executiveId")
    @Mapping(target="car.id", source="carId")
    CreditRequestModel toDomain(CreditRequestUpdateRequest source);
    CreditRequestModel toDomain(CreditRequestPatchRequest source);
}
