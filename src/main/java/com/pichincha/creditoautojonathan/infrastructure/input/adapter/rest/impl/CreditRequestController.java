package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.impl;

import com.pichincha.creditoautojonathan.application.input.port.CreditRequestService;
import com.pichincha.creditoautojonathan.commons.CustomResponse;
import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.logs.LogsHelper;
import com.pichincha.creditoautojonathan.commons.logs.enums.OperationType;
import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.CreditRequestModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest.CreditRequestCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest.CreditRequestPatchRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest.CreditRequestUpdateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper.CreditRequestMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.CREDIT_CONTROLLER_PATH)
@Tag(name = "Solicitud de Crédito Controller", description = "Controlador de metodos para la gestion de solicitud de credito")
public class CreditRequestController {

    private final CreditRequestService creditRequestService;

    private final CreditRequestMapper creditRequestMapper;

    private final GlobalProperties globalProperties;

    private final CustomResponse customResponse;

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = CreditRequestModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getAllCreditRequest", description = "Obtiene info de los creditos solicitados")
    @GetMapping(value = Constants.CREDIT_CONTROLLER_GET_ALL_PATH)
    public ResponseEntity<CustomResponse> getAllCreditRequest() {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CREDIT_GET_ALL, globalProperties);

        final List<CreditRequestModel> brandModels = creditRequestService.getAll();
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(brandModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CREDIT_GET_ALL, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CreditRequestModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getCreditRequest", description = "Obtiene info del patio")
    @GetMapping(value = Constants.CREDIT_CONTROLLER_GET_PATH)
    public ResponseEntity<CustomResponse> getCreditRequest(@Valid @PathVariable("requestId") Integer requestId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CREDIT_GET, globalProperties);

        final CreditRequestModel data = creditRequestService.get(requestId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(data);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CREDIT_GET, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CreditRequestModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "createCreditRequest", description = "Registra una nueva solicitud de crédito")
    @PostMapping(value = Constants.CREDIT_CONTROLLER_POST_PATH)
    public ResponseEntity<CustomResponse> createCreditRequest(@RequestBody CreditRequestCreateRequest creditRequestCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(creditRequestCreateRequest, OperationType.CREDIT_POST_REQUEST, globalProperties);

        CreditRequestModel domain = creditRequestMapper.toDomain(creditRequestCreateRequest);
        domain = creditRequestService.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CREDIT_POST_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CreditRequestModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "updateCreditRequest", description = "Actualiza una solicitud de crédito")
    @PutMapping(value = Constants.CREDIT_CONTROLLER_PUT_PATH)
    public ResponseEntity<CustomResponse> updateCreditRequest(@RequestBody CreditRequestUpdateRequest creditRequestUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(creditRequestUpdateRequest, OperationType.CREDIT_PUT_REQUEST, globalProperties);

        CreditRequestModel domain = creditRequestMapper.toDomain(creditRequestUpdateRequest);
        domain = creditRequestService.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CREDIT_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CreditRequestModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "patchCreditRequest", description = "Actualiza el estado de solicitud de crédito")
    @PatchMapping(value = Constants.CREDIT_CONTROLLER_PATCH_PATH)
    public ResponseEntity<CustomResponse> patchCreditRequest(@RequestBody CreditRequestPatchRequest creditRequestPatchRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(creditRequestPatchRequest, OperationType.CREDIT_PATCH_REQUEST, globalProperties);

        CreditRequestModel domain = creditRequestMapper.toDomain(creditRequestPatchRequest);
        domain = creditRequestService.patch(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CREDIT_PATCH_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "deleteCreditRequest", description = "Elimina una solicitud de crédito")
    @DeleteMapping(value = Constants.CREDIT_CONTROLLER_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteCreditRequest(@Valid @PathVariable("requestId") Integer requestId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CREDIT_DELETE_REQUEST, globalProperties);

        creditRequestService.delete(requestId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CREDIT_DELETE_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(responseSuccess);
    }
}
