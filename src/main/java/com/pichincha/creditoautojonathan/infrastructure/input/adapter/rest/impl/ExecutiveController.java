package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.impl;

import com.pichincha.creditoautojonathan.application.input.port.ExecutiveService;
import com.pichincha.creditoautojonathan.commons.CustomResponse;
import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.logs.LogsHelper;
import com.pichincha.creditoautojonathan.commons.logs.enums.OperationType;
import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.executive.ExecutiveCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.executive.ExecutiveUpdateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper.ExecutiveMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.EXECUTIVE_CONTROLLER_PATH)
@Tag(name = "Ejecutivos Controller", description = "Controlador de metodos para la gestion de ejecutivos")
public class ExecutiveController {

    private final ExecutiveService executiveService;

    private final ExecutiveMapper executiveMapper;

    private final GlobalProperties globalProperties;

    private final CustomResponse customResponse;

    @PostConstruct
    public void loadDataExecutive() {
        executiveService.loadData();
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = ExecutiveModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "getAllExecutive", description = "Obtiene info de los ejecutivos")
    @GetMapping(value = Constants.EXECUTIVE_CONTROLLER_GET_ALL_PATH)
    public ResponseEntity<CustomResponse> getAllExecutive() {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.EXECUTIVE_GET_ALL, globalProperties);

        final List<ExecutiveModel> executiveModels = executiveService.getAll();
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(executiveModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.EXECUTIVE_GET_ALL, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ExecutiveModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getExecutive", description = "Obtiene info del ejecutivo")
    @GetMapping(value = Constants.EXECUTIVE_CONTROLLER_GET_PATH)
    public ResponseEntity<CustomResponse> getExecutive(@Valid @PathVariable("executiveId") Integer executiveId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.EXECUTIVE_GET, globalProperties);

        final ExecutiveModel data = executiveService.get(executiveId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(data);

        LogsHelper.getLogEnd(responseSuccess, OperationType.EXECUTIVE_GET, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ExecutiveModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "createExecutive", description = "Registra un nuevo ejecutivo")
    @PostMapping(value = Constants.EXECUTIVE_CONTROLLER_POST_PATH)
    public ResponseEntity<CustomResponse> createExecutive(@RequestBody ExecutiveCreateRequest executiveCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(executiveCreateRequest, OperationType.EXECUTIVE_POST_REQUEST, globalProperties);

        ExecutiveModel domain = executiveMapper.toDomain(executiveCreateRequest);
        domain = executiveService.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.EXECUTIVE_POST_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ExecutiveModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "updateExecutive", description = "Actualiza un ejecutivo")
    @PutMapping(value = Constants.EXECUTIVE_CONTROLLER_PUT_PATH)
    public ResponseEntity<CustomResponse> updateExecutive(@RequestBody ExecutiveUpdateRequest executiveUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(executiveUpdateRequest, OperationType.EXECUTIVE_PUT_REQUEST, globalProperties);

        ExecutiveModel domain = executiveMapper.toDomain(executiveUpdateRequest);
        domain = executiveService.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.EXECUTIVE_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "deleteExecutive", description = "Elimina un ejecutivo")
    @DeleteMapping(value = Constants.EXECUTIVE_CONTROLLER_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteExecutive(@Valid @PathVariable("executiveId") Integer executiveId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.EXECUTIVE_DELETE_REQUEST, globalProperties);

        executiveService.delete(executiveId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.EXECUTIVE_DELETE_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(responseSuccess);
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = ExecutiveModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "getAllExecutiveYard", description = "Obtiene info de los ejecutivos por patio")
    @GetMapping(value = Constants.EXECUTIVE_CONTROLLER_GET_ALL_BY_YARD_PATH)
    public ResponseEntity<CustomResponse> getAllExecutiveYard(@Valid @PathVariable("yardId") Integer yardId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.EXECUTIVE_GET_ALL_BY_YARD, globalProperties);

        final List<ExecutiveModel> executiveModels = executiveService.getAllYard(yardId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(executiveModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.EXECUTIVE_GET_ALL_BY_YARD, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
}
