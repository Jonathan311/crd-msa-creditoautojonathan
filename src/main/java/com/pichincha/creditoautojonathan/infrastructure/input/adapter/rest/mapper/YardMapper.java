package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper;

import com.pichincha.creditoautojonathan.domain.model.YardModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.yard.YardCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.yard.YardUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface YardMapper {
    YardModel toDomain(YardCreateRequest source);
    YardModel toDomain(YardUpdateRequest source);
}
