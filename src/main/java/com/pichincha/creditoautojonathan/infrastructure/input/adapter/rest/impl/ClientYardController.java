package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.impl;

import com.pichincha.creditoautojonathan.application.input.port.ClientYardService;
import com.pichincha.creditoautojonathan.commons.CustomResponse;
import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.logs.LogsHelper;
import com.pichincha.creditoautojonathan.commons.logs.enums.OperationType;
import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.clientyard.ClientYardCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.clientyard.ClientYardUpdateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper.ClientYardMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.CLIENT_YARD_CONTROLLER_PATH)
@Tag(name = "Cliente por Patio Controller", description = "Controlador de metodos para la gestion de clientes por de patios de autos")
public class ClientYardController {

    private final ClientYardService clientYardService;

    private final ClientYardMapper clientYardMapper;

    private final GlobalProperties globalProperties;

    private final CustomResponse customResponse;

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = ClientYardModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getAllClientYard", description = "Obtiene info de los clientes por patios de autos")
    @GetMapping(value = Constants.CLIENT_YARD_CONTROLLER_GET_ALL_PATH)
    public ResponseEntity<CustomResponse> getAllClientYard() {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENT_YARD_GET_ALL, globalProperties);

        final List<ClientYardModel> brandModels = clientYardService.getAll();
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(brandModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_YARD_GET_ALL, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ClientYardModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getClientYard", description = "Obtiene info del cliente por patio")
    @GetMapping(value = Constants.CLIENT_YARD_CONTROLLER_GET_PATH)
    public ResponseEntity<CustomResponse> getClientYard(@Valid @PathVariable("clientYardId") Integer clientYardId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENT_YARD_GET, globalProperties);

        final ClientYardModel data = clientYardService.get(clientYardId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(data);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_YARD_GET, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ClientYardModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "createClientYard", description = "Registra un nuevo cliente por patio")
    @PostMapping(value = Constants.CLIENT_YARD_CONTROLLER_POST_PATH)
    public ResponseEntity<CustomResponse> createClientYard(@RequestBody ClientYardCreateRequest clientYardCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(clientYardCreateRequest, OperationType.CLIENT_YARD_POST_REQUEST, globalProperties);

        ClientYardModel domain = clientYardMapper.toDomain(clientYardCreateRequest);
        domain = clientYardService.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_YARD_POST_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = ClientYardModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "updateClientYard", description = "Actualiza un cliente por patio")
    @PutMapping(value = Constants.CLIENT_YARD_CONTROLLER_PUT_PATH)
    public ResponseEntity<CustomResponse> updateClientYard(@RequestBody ClientYardUpdateRequest clientYardUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(clientYardUpdateRequest, OperationType.CLIENT_YARD_PUT_REQUEST, globalProperties);

        ClientYardModel domain = clientYardMapper.toDomain(clientYardUpdateRequest);
        domain = clientYardService.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_YARD_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "deleteClientYard", description = "Elimina un cliente por patio")
    @DeleteMapping(value = Constants.CLIENT_YARD_CONTROLLER_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteClientYard(@Valid @PathVariable("clientYardId") Integer clientYardId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CLIENT_YARD_DELETE_REQUEST, globalProperties);

        clientYardService.delete(clientYardId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CLIENT_YARD_DELETE_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(responseSuccess);
    }
}
