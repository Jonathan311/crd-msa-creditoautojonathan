package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.creditRequest;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Slf4j
@Getter
@Setter
@Tag(name = "CreditRequestPatchRequest", description = "Solicitud de actualizar un crédito")
public class CreditRequestPatchRequest extends SelfValidating<CreditRequestPatchRequest> {

    @Schema(description = "ID del crédito", required = true)
    @NotNull(message = "{id.null}")
    private Long id;

    @Schema(description = "Estado del crédito", required = true)
    @Pattern(regexp = "R|D|C", message = "{status.mask}")
    @NotEmpty(message = "{status.empty}")
    @NotNull(message = "{status.null}")
    private String status;

    @Schema(description = "Observación")
    @Size(max = 255, message = "{observation.size}")
    private String observation;

    public CreditRequestPatchRequest(Long id, String status,
                                     String observation) {
        this.id = id;
        this.status = status;
        this.observation = observation;
        this.validateSelf();
    }
}
