package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.car;

import com.pichincha.creditoautojonathan.commons.SelfValidating;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Slf4j
@Getter
@Setter
@Tag(name = "CarCreateRequest", description = "Solicitud de crear un vehiculo")
public class CarCreateRequest extends SelfValidating<CarCreateRequest> {

    @Schema(description = "Placa del vehiculo", required = true)
    @NotEmpty(message = "{plate.empty}")
    @NotNull(message = "{plate.null}")
    @Size(min = 1, max = 50, message = "{plate.size}")
    private String plate;

    @Schema(description = "Modelo del vehiculo", required = true)
    @NotEmpty(message = "{model.empty}")
    @NotNull(message = "{model.null}")
    @Size(min = 1, max = 10, message = "{model.size}")
    private String model;

    @Schema(description = "Número de chasis del vehiculo", required = true)
    @NotEmpty(message = "{numberChassis.empty}")
    @NotNull(message = "{numberChassis.null}")
    @Size(min = 1, max = 255, message = "{numberChassis.size}")
    private String numberChassis;

    @Schema(description = "Tipo de vehiculo")
    @Size(max = 50, message = "{type.size}")
    private String type;

    @Schema(description = "Cilindraje del vehiculo", required = true)
    @NotEmpty(message = "{cylindrical.empty}")
    @NotNull(message = "{cylindrical.null}")
    @Size(min = 1, max = 50, message = "{cylindrical.size}")
    private String cylindrical;

    @Schema(description = "Avaluo del vehiculo", required = true)
    @NotEmpty(message = "{appraisal.empty}")
    @NotNull(message = "{appraisal.null}")
    @Size(min = 1, max = 50, message = "{appraisal.size}")
    private String appraisal;

    @Schema(description = "Estado del vehiculo", required = true)
    @Pattern(regexp = "A|D", message = "{status.mask}")
    @NotEmpty(message = "{status.empty}")
    @NotNull(message = "{status.null}")
    @Size(min = 1, max = 1, message = "{status.size}")
    private String status;

    @Schema(description = "Marca del vehiculo", required = true)
    @NotNull(message = "{brandId.null}")
    private Long brandId;

    public CarCreateRequest(String plate, String model, String numberChassis, String type, String cylindrical, String appraisal, String status, Long brandId) {
        this.plate = plate;
        this.model = model;
        this.numberChassis = numberChassis;
        this.type = type;
        this.cylindrical = cylindrical;
        this.appraisal = appraisal;
        this.status = status;
        this.brandId = brandId;
        this.validateSelf();
    }
}