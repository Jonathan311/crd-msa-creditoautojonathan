package com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.impl;

import com.pichincha.creditoautojonathan.application.input.port.CarService;
import com.pichincha.creditoautojonathan.commons.CustomResponse;
import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.logs.LogsHelper;
import com.pichincha.creditoautojonathan.commons.logs.enums.OperationType;
import com.pichincha.creditoautojonathan.commons.properties.GlobalProperties;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.car.CarCreateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.command.request.car.CarUpdateRequest;
import com.pichincha.creditoautojonathan.infrastructure.input.adapter.rest.mapper.CarMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.SchemaProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@CrossOrigin(origins = Constants.APP_SAFE, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping(Constants.CAR_CONTROLLER_PATH)
@Tag(name = "Vehiculo Controller", description = "Controlador de metodos para la gestion de vehiculos")
public class CarController {

    private final CarService carService;

    private final CarMapper carMapper;

    private final GlobalProperties globalProperties;

    private final CustomResponse customResponse;

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = CarModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getAllCar", description = "Obtiene info de los vehiculos")
    @GetMapping(value = Constants.CAR_CONTROLLER_GET_ALL_PATH)
    public ResponseEntity<CustomResponse> getAllCar() {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CAR_GET_ALL, globalProperties);

        final List<CarModel> brandModels = carService.getAll();
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(brandModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CAR_GET_ALL, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CarModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getCar", description = "Obtiene info del vehiculo")
    @GetMapping(value = Constants.CAR_CONTROLLER_GET_PATH)
    public ResponseEntity<CustomResponse> getCar(@Valid @PathVariable("carId") Integer carId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CAR_GET, globalProperties);

        final CarModel data = carService.get(carId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(data);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CAR_GET, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CarModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "createCar", description = "Registra un nuevo vehiculo")
    @PostMapping(value = Constants.CAR_CONTROLLER_POST_PATH)
    public ResponseEntity<CustomResponse> createCar(@RequestBody CarCreateRequest carCreateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(carCreateRequest, OperationType.CAR_POST_REQUEST, globalProperties);

        CarModel domain = carMapper.toDomain(carCreateRequest);
        domain = carService.create(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CAR_POST_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", schema = @Schema(implementation = CarModel.class)), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "updateCar", description = "Actualiza un vehiculo")
    @PutMapping(value = Constants.CAR_CONTROLLER_PUT_PATH)
    public ResponseEntity<CustomResponse> updateCar(@RequestBody CarUpdateRequest carUpdateRequest) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(carUpdateRequest, OperationType.CAR_PUT_REQUEST, globalProperties);

        CarModel domain = carMapper.toDomain(carUpdateRequest);
        domain = carService.update(domain);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(domain);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CAR_PUT_REQUEST, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
    @ApiResponse(responseCode = "200", description = "Success", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})
    @Operation(summary = "deleteCar", description = "Elimina un vehiculo")
    @DeleteMapping(value = Constants.CAR_CONTROLLER_DELETE_PATH)
    public ResponseEntity<CustomResponse> deleteCar(@Valid @PathVariable("carId") Integer carId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CAR_DELETE_REQUEST, globalProperties);

        carService.delete(carId);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(null);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CAR_DELETE_REQUEST, globalProperties);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(responseSuccess);
    }

    @ApiResponse(responseCode = "200", description = "Success", content = {
            @Content(mediaType = "application/json", schemaProperties = @SchemaProperty(name = "data", array = @ArraySchema(schema = @Schema(oneOf = CarModel.class))), schema = @Schema(oneOf = {CustomResponseCode.class}))})
    @ApiResponse(responseCode = "400", description = "Bad Request", content = {@Content(mediaType = "application/json", schema = @Schema(implementation = CustomResponseCode.class))})

    @Operation(summary = "getAllCarSearch", description = "Obtiene info de los vehiculos por filtro")
    @GetMapping(value = Constants.CAR_CONTROLLER_GET_ALL_FILTER_PATH)
    public ResponseEntity<CustomResponse> getAllCarSearch(@RequestParam(required = false) String model, @RequestParam(required = false) Long branId) {

        UtilsHelper.assignCorrelative(globalProperties);
        LogsHelper.getLogStart(null, OperationType.CAR_GET_ALL_FILTER, globalProperties);

        final List<CarModel> brandModels = carService.getBrandOrModel(branId, model);
        final CustomResponse responseSuccess = customResponse.buildSuccessResponse(brandModels);

        LogsHelper.getLogEnd(responseSuccess, OperationType.CAR_GET_ALL_FILTER, globalProperties);
        return ResponseEntity.ok().body(responseSuccess);
    }
}
