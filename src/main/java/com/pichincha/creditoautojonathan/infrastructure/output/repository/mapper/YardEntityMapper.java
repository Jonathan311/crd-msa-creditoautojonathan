package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.YardModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.YardEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface YardEntityMapper {
    List<YardModel> toDomain(List<YardEntity> yardEntities);
    YardModel toDomain(YardEntity yardEntity);
    List<YardEntity> toEntity(List<YardModel> yardModels);

    YardEntity toEntity(YardModel yardModel);
}
