package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.BrandRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.BrandEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.BrandJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.BrandEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class BrandRepositoryImpl implements BrandRepositoryPort {

    private final BrandJpaRepository brandJpaRepository;

    private final BrandEntityMapper brandEntityMapper;

    @Override
    public void loadData(List<BrandModel> brandModels) {
        log.info("Start loadData Brand");
        List<BrandEntity> brandEntities = brandEntityMapper.toEntity(brandModels);
        brandJpaRepository.saveAll(brandEntities);
        log.info("End loadData Brand");
    }

    @Override
    public List<BrandModel> getAll() {
        log.info("Start getAll Brand");
        List<BrandEntity> brandEntities = brandJpaRepository.findAll();
        log.info("End getAll Brand");
        return brandEntityMapper.toDomain(brandEntities);
    }

    @Override
    public BrandModel get(Integer id) {
        log.info("Start getBrand [{}]", id);
        final Optional<BrandEntity> brandEntity = brandJpaRepository.findById(Long.valueOf(id));
        if (brandEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_BRAND.name());
        }
        return brandEntityMapper.toDomain(brandEntity.get());
    }
}
