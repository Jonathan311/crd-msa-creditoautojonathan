package com.pichincha.creditoautojonathan.infrastructure.output.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Builder
@Data
@Entity
@Table(name = "client")
@NoArgsConstructor
@AllArgsConstructor
public class ClientEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String identification;

    private String name;

    private Integer age;

    private Date birthdate;

    private String surname;

    private String address;

    private String phone;

    @Column(name="civil_status")
    private String civilStatus;

    @Column(name="spouse_identification")
    private String spouseIdentification;

    @Column(name="spouse_name")
    private String spouseName;

    @Column(name="credit_subject")
    private Boolean creditSubject;
}
