package com.pichincha.creditoautojonathan.infrastructure.output.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Builder
@Data
@Entity
@Table(name = "client_yard")
@NoArgsConstructor
@AllArgsConstructor
public class ClientYardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_client")
    private ClientEntity client;

    @ManyToOne
    @JoinColumn(name = "id_yard")
    private YardEntity yard;

    @Column(name="date_assignment")
    private Date dateAssignment;
}
