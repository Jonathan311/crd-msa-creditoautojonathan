package com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa;

import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientJpaRepository extends JpaRepository<ClientEntity, Long> {
}
