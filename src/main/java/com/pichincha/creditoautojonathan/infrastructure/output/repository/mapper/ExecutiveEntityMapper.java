package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ExecutiveEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ExecutiveEntityMapper {
    List<ExecutiveModel> toDomain(List<ExecutiveEntity> executiveEntities);
    ExecutiveModel toDomain(ExecutiveEntity executiveEntity);
    List<ExecutiveEntity> toEntity(List<ExecutiveModel> yardModels);

    ExecutiveEntity toEntity(ExecutiveModel yardModel);
}
