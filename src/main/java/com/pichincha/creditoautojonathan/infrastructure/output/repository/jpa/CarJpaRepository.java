package com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa;

import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarJpaRepository extends JpaRepository<CarEntity, Long>, JpaSpecificationExecutor<CarEntity> {
    List<CarEntity> findByPlateAndIdNot(String plate, Long id);
}
