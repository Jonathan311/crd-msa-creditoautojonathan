package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientYardEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ClientYardEntityMapper {
    List<ClientYardModel> toDomain(List<ClientYardEntity> clientYardEntities);
    ClientYardModel toDomain(ClientYardEntity clientYardEntity);
    List<ClientYardEntity> toEntity(List<ClientYardModel> clientYardModels);

    ClientYardEntity toEntity(ClientYardModel clientYardModel);
}
