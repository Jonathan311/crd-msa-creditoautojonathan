package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.CreditRequestModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CreditRequestEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CreditRequestEntityMapper {
    List<CreditRequestModel> toDomain(List<CreditRequestEntity> creditRequestEntities);
    CreditRequestModel toDomain(CreditRequestEntity creditRequestEntity);
    List<CreditRequestEntity> toEntity(List<CreditRequestModel> creditRequestModels);

    CreditRequestEntity toEntity(CreditRequestModel creditRequestModel);
}
