package com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa;

import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CarEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CreditRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface CreditRequestJpaRepository extends JpaRepository<CreditRequestEntity, Long> {
    CreditRequestEntity findByIdNotAndClientAndStatusAndDateElaboration(Long id, ClientEntity client, String status, LocalDate dateElaboration);

    CreditRequestEntity findByIdNotAndCarAndStatus(Long id, CarEntity car, String status);
}
