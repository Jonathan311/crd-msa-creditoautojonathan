package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.ClientYardRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientYardEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.ClientYardJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.ClientYardEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class ClientYardRepositoryImpl implements ClientYardRepositoryPort {

    private final ClientYardJpaRepository clientYardJpaRepository;

    private final ClientYardEntityMapper clientYardEntityMapper;

    @Override
    public List<ClientYardModel> getAll() {
        log.info("Start getAll ClientYard");
        List<ClientYardEntity> clientYardEntities = clientYardJpaRepository.findAll();
        log.info("End getAll ClientYard");
        return clientYardEntityMapper.toDomain(clientYardEntities);
    }

    @Override
    public ClientYardModel get(Integer id) {
        log.info("Start getClientYard [{}]", id);
        final Optional<ClientYardEntity> clientYardEntity = clientYardJpaRepository.findById(Long.valueOf(id));
        if (clientYardEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_CLIENT_YARD.name());
        }
        return clientYardEntityMapper.toDomain(clientYardEntity.get());
    }

    @Override
    public ClientYardModel create(ClientYardModel clientYardModel) {
        try {
            log.info("Start createClientYard [{}]", clientYardModel.protectedToString());
            final ClientYardEntity clientYardEntity = clientYardEntityMapper.toEntity(clientYardModel);
            return clientYardEntityMapper.toDomain(clientYardJpaRepository.save(clientYardEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_CLIENTYARD:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public ClientYardModel update(ClientYardModel clientYardModel) {
        try {
            log.info("Start updateClientYard [{}]", clientYardModel.protectedToString());
            final ClientYardEntity clientYardEntity = clientYardEntityMapper.toEntity(clientYardModel);
            return clientYardEntityMapper.toDomain(clientYardJpaRepository.save(clientYardEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_CLIENTYARD:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(ClientYardModel clientYardModel) {
        try {
            log.info("Start deleteClientYard [{}]", clientYardModel.protectedToString());
            final ClientYardEntity clientYardEntity = clientYardEntityMapper.toEntity(clientYardModel);
            clientYardJpaRepository.delete(clientYardEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_CLIENTYARD:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
