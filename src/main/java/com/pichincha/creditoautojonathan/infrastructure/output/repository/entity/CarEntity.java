package com.pichincha.creditoautojonathan.infrastructure.output.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@Entity
@Table(name = "car")
@NoArgsConstructor
@AllArgsConstructor
public class CarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String plate;

    private String model;

    @Column(name="number_chassis")
    private String numberChassis;

    private String type;

    private String cylindrical;

    private String appraisal;

    private String status;

    @ManyToOne
    @JoinColumn(name = "id_brand")
    private BrandEntity brand;
}
