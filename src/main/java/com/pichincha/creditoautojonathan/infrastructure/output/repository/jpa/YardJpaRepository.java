package com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa;

import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.YardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface YardJpaRepository extends JpaRepository<YardEntity, Long> {
}
