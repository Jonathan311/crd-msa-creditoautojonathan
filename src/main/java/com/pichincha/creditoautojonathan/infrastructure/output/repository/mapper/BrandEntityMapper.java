package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.BrandEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface BrandEntityMapper {
    List<BrandModel> toDomain(List<BrandEntity> brandEntities);
    BrandModel toDomain(BrandEntity brandEntity);
    List<BrandEntity> toEntity(List<BrandModel> brandModels);

    BrandEntity toEntity(BrandModel brandModel);
}
