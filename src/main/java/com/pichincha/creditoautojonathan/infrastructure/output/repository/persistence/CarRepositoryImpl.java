package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.CarRepositoryPort;
import com.pichincha.creditoautojonathan.commons.specificationconfig.GenericSpecificationsBuilder;
import com.pichincha.creditoautojonathan.commons.specificationconfig.SpecificationFactory;
import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CarEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.CarJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.CarEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class CarRepositoryImpl implements CarRepositoryPort {

    private final CarJpaRepository carJpaRepository;

    private final CarEntityMapper carEntityMapper;

    private final SpecificationFactory<CarEntity> carEntitySpecificationFactory;

    @Override
    public List<CarModel> getAll() {
        log.info("Start getAll Car");
        List<CarEntity> carEntities = carJpaRepository.findAll();
        log.info("End getAll Car");
        return carEntityMapper.toDomain(carEntities);
    }

    @Override
    public CarModel get(Integer id) {
        log.info("Start getCar [{}]", id);
        final Optional<CarEntity> carEntity = carJpaRepository.findById(Long.valueOf(id));
        if (carEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_CAR.name());
        }
        return carEntityMapper.toDomain(carEntity.get());
    }

    @Override
    public CarModel create(CarModel carModel) {
        try {
            log.info("Start createCar [{}]", carModel.protectedToString());
            final CarEntity carEntity = carEntityMapper.toEntity(carModel);
            return carEntityMapper.toDomain(carJpaRepository.save(carEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_CAR:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public CarModel update(CarModel carModel) {
        try {
            log.info("Start updateCar [{}]", carModel.protectedToString());
            final CarEntity carEntity = carEntityMapper.toEntity(carModel);
            return carEntityMapper.toDomain(carJpaRepository.save(carEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_CAR:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(CarModel carModel) {
        try {
            log.info("Start deleteCar [{}]", carModel.protectedToString());
            final CarEntity carEntity = carEntityMapper.toEntity(carModel);
            carJpaRepository.delete(carEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_CAR:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }

    @Override
    public List<CarModel> getPlate(String plate, Long id) {
        log.info("Start getPlate [{}]", plate);
        final List<CarEntity> carEntity = carJpaRepository.findByPlateAndIdNot(plate, id);
        return carEntityMapper.toDomain(carEntity);
    }

    @Override
    public List<CarModel> getBrandOrModel(Long id, String model) {
        log.info("Start getBrandOrModel [{}]", model);

        GenericSpecificationsBuilder<CarEntity> builder = new GenericSpecificationsBuilder<>();

        if (StringUtils.isNotEmpty(model)) {
            builder.with(carEntitySpecificationFactory.isEqual("model", model));
        }
        if (!Objects.isNull(id)) {
            List<Object> list = new ArrayList<>();
            list.add(id);
            list.add("brand");
            builder.with(carEntitySpecificationFactory.isJoinEquality("id", list));
        }

        List<CarEntity> carEntities = carJpaRepository.findAll(builder.build());

        return carEntityMapper.toDomain(carEntities);
    }
}
