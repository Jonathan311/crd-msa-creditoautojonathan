package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.ClientRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.ClientJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.ClientEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class ClientRepositoryImpl implements ClientRepositoryPort {

    private final ClientJpaRepository clientJpaRepository;

    private final ClientEntityMapper clientEntityMapper;

    @Override
    public void loadData(List<ClientModel> clientModels) {
        log.info("Start loadData Client");
        List<ClientEntity> clientEntities = clientEntityMapper.toEntity(clientModels);
        clientJpaRepository.saveAll(clientEntities);
        log.info("End loadData Client");
    }

    @Override
    public List<ClientModel> getAll() {
        log.info("Start getAll Client");
        List<ClientEntity> clientEntities = clientJpaRepository.findAll();
        log.info("End getAll Client");
        return clientEntityMapper.toDomain(clientEntities);
    }

    @Override
    public ClientModel get(Integer id) {
        log.info("Start getClient [{}]", id);
        final Optional<ClientEntity> clientEntity = clientJpaRepository.findById(Long.valueOf(id));
        if (clientEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_CLIENT.name());
        }
        return clientEntityMapper.toDomain(clientEntity.get());
    }

    @Override
    public ClientModel create(ClientModel clientModel) {
        try {
            log.info("Start createClient [{}]", clientModel.protectedToString());
            final ClientEntity clientEntity = clientEntityMapper.toEntity(clientModel);
            return clientEntityMapper.toDomain(clientJpaRepository.save(clientEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_CLIENT:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public ClientModel update(ClientModel clientModel) {
        try {
            log.info("Start updateClient [{}]", clientModel.protectedToString());
            final ClientEntity clientEntity = clientEntityMapper.toEntity(clientModel);
            return clientEntityMapper.toDomain(clientJpaRepository.save(clientEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_CLIENT:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(ClientModel clientModel) {
        try {
            log.info("Start deleteClient [{}]", clientModel.protectedToString());
            final ClientEntity clientEntity = clientEntityMapper.toEntity(clientModel);
            clientJpaRepository.delete(clientEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_CLIENT:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
