package com.pichincha.creditoautojonathan.infrastructure.output.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@Entity
@Table(name = "executive")
@NoArgsConstructor
@AllArgsConstructor
public class ExecutiveEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String identification;

    private String name;

    private String surname;

    private String address;

    @Column(name="conventional_telephone")
    private String conventionalTelephone;

    @Column(name="cell_phone")
    private String cellPhone;

    @ManyToOne
    @JoinColumn(name = "id_yard")
    private YardEntity yard;

    private Integer age;
}
