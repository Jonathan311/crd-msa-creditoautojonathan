package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ClientEntityMapper {
    List<ClientModel> toDomain(List<ClientEntity> clientEntities);
    ClientModel toDomain(ClientEntity clientEntity);
    List<ClientEntity> toEntity(List<ClientModel> clientModels);

    ClientEntity toEntity(ClientModel clientModel);
}
