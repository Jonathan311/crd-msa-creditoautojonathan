package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.YardRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.YardEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.YardJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.YardEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class YardRepositoryImpl implements YardRepositoryPort {

    private final YardJpaRepository yardJpaRepository;

    private final YardEntityMapper yardEntityMapper;

    @Override
    public List<YardModel> getAll() {
        log.info("Start getAll Yard");
        List<YardEntity> yardEntities = yardJpaRepository.findAll();
        log.info("End getAll Yard");
        return yardEntityMapper.toDomain(yardEntities);
    }

    @Override
    public YardModel get(Integer id) {
        log.info("Start getYard [{}]", id);
        final Optional<YardEntity> yardEntity = yardJpaRepository.findById(Long.valueOf(id));
        if (yardEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_YARD.name());
        }
        return yardEntityMapper.toDomain(yardEntity.get());
    }

    @Override
    public YardModel create(YardModel yardModel) {
        try {
            log.info("Start createYard [{}]", yardModel.protectedToString());
            final YardEntity yardEntity = yardEntityMapper.toEntity(yardModel);
            return yardEntityMapper.toDomain(yardJpaRepository.save(yardEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_YARD:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public YardModel update(YardModel yardModel) {
        try {
            log.info("Start updateYard [{}]", yardModel.protectedToString());
            final YardEntity yardEntity = yardEntityMapper.toEntity(yardModel);
            return yardEntityMapper.toDomain(yardJpaRepository.save(yardEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_YARD:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(YardModel yardModel) {
        try {
            log.info("Start deleteYard [{}]", yardModel.protectedToString());
            final YardEntity yardEntity = yardEntityMapper.toEntity(yardModel);
            yardJpaRepository.delete(yardEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_YARD:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
