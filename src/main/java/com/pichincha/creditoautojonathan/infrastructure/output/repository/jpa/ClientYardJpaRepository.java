package com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa;

import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientYardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientYardJpaRepository extends JpaRepository<ClientYardEntity, Long> {
}
