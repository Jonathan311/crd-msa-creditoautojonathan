package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.ExecutiveRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ExecutiveEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.YardEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.ExecutiveJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.ExecutiveEntityMapper;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.YardEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class ExecutiveRepositoryImpl implements ExecutiveRepositoryPort {

    private final ExecutiveJpaRepository executiveJpaRepository;

    private final ExecutiveEntityMapper executiveEntityMapper;

    private final YardEntityMapper yardEntityMapper;

    @Override
    public void loadData(List<ExecutiveModel> executiveModels) {
        log.info("Start loadData Executive");
        List<ExecutiveEntity> executiveEntities = executiveEntityMapper.toEntity(executiveModels);
        executiveJpaRepository.saveAll(executiveEntities);
        log.info("End loadData Executive");
    }

    @Override
    public List<ExecutiveModel> getAll() {
        log.info("Start getAll Executive");
        List<ExecutiveEntity> executiveEntities = executiveJpaRepository.findAll();
        log.info("End getAll Executive");
        return executiveEntityMapper.toDomain(executiveEntities);
    }

    @Override
    public List<ExecutiveModel> getAllYard(YardModel yardModel) {
        log.info("Start getAllYard Executive");
        YardEntity yardEntity = yardEntityMapper.toEntity(yardModel);
        List<ExecutiveEntity> executiveEntities = executiveJpaRepository.findAllByYard(yardEntity);
        log.info("End getAllYard Executive");
        return executiveEntityMapper.toDomain(executiveEntities);
    }

    @Override
    public ExecutiveModel get(Integer id) {
        log.info("Start getExecutive [{}]", id);
        final Optional<ExecutiveEntity> executiveEntity = executiveJpaRepository.findById(Long.valueOf(id));
        if (executiveEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_EXECUTIVE.name());
        }
        return executiveEntityMapper.toDomain(executiveEntity.get());
    }

    @Override
    public ExecutiveModel create(ExecutiveModel yardModel) {
        try {
            log.info("Start createExecutive [{}]", yardModel.protectedToString());
            final ExecutiveEntity executiveEntity = executiveEntityMapper.toEntity(yardModel);
            return executiveEntityMapper.toDomain(executiveJpaRepository.save(executiveEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_EXECUTIVE:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public ExecutiveModel update(ExecutiveModel executiveModel) {
        try {
            log.info("Start updateExecutive [{}]", executiveModel.protectedToString());
            final ExecutiveEntity executiveEntity = executiveEntityMapper.toEntity(executiveModel);
            return executiveEntityMapper.toDomain(executiveJpaRepository.save(executiveEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_EXECUTIVE:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(ExecutiveModel executiveModel) {
        try {
            log.info("Start deleteExecutive [{}]", executiveModel.protectedToString());
            final ExecutiveEntity executiveEntity = executiveEntityMapper.toEntity(executiveModel);
            executiveJpaRepository.delete(executiveEntity);
        }catch(Exception ex){
            log.warn("ERROR_DELETE_EXECUTIVE:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }
}
