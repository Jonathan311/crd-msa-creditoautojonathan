package com.pichincha.creditoautojonathan.infrastructure.output.repository.persistence;

import com.pichincha.creditoautojonathan.application.output.port.CreditRequestRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.domain.model.CreditRequestModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CarEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ClientEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CreditRequestEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa.CreditRequestJpaRepository;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.CarEntityMapper;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.ClientEntityMapper;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper.CreditRequestEntityMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Component
public class CreditRequestRepositoryImpl implements CreditRequestRepositoryPort {

    private final CreditRequestJpaRepository creditRequestJpaRepository;

    private final CreditRequestEntityMapper creditRequestEntityMapper;

    private final ClientEntityMapper clientEntityMapper;

    private final CarEntityMapper carEntityMapper;

    @Override
    public List<CreditRequestModel> getAll() {
        log.info("Start getAll CreditRequest");
        List<CreditRequestEntity> creditRequestEntities = creditRequestJpaRepository.findAll();
        log.info("End getAll CreditRequest");
        return creditRequestEntityMapper.toDomain(creditRequestEntities);
    }

    @Override
    public CreditRequestModel get(Integer id) {
        log.info("Start getCreditRequest [{}]", id);
        final Optional<CreditRequestEntity> creditRequestEntity = creditRequestJpaRepository.findById(Long.valueOf(id));
        if (creditRequestEntity.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_NOT_FOUND_CREDIT_REQUEST.name());
        }
        return creditRequestEntityMapper.toDomain(creditRequestEntity.get());
    }

    @Override
    public CreditRequestModel create(CreditRequestModel creditRequestModel) {
        try {
            log.info("Start createCreditRequest [{}]", creditRequestModel.protectedToString());
            final CreditRequestEntity creditRequestEntity = creditRequestEntityMapper.toEntity(creditRequestModel);
            return creditRequestEntityMapper.toDomain(creditRequestJpaRepository.save(creditRequestEntity));
        } catch (Exception ex) {
            log.warn("ERROR_CREATE_CreditRequest:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public CreditRequestModel update(CreditRequestModel creditRequestModel) {
        try {
            log.info("Start updateCreditRequest [{}]", creditRequestModel.protectedToString());
            final CreditRequestEntity creditRequestEntity = creditRequestEntityMapper.toEntity(creditRequestModel);
            return creditRequestEntityMapper.toDomain(creditRequestJpaRepository.save(creditRequestEntity));
        } catch (Exception ex) {
            log.warn("ERROR_UPDATE_CreditRequest:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_DUPLICATE.name());
        }
    }

    @Override
    public void delete(CreditRequestModel creditRequestModel) {
        try {
            log.info("Start deleteCreditRequest [{}]", creditRequestModel.protectedToString());
            final CreditRequestEntity creditRequestEntity = creditRequestEntityMapper.toEntity(creditRequestModel);
            creditRequestJpaRepository.delete(creditRequestEntity);
        } catch (Exception ex) {
            log.warn("ERROR_DELETE_CreditRequest:[{}]", ex.getMessage());
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_INTEGRITY_VIOLATION_FOREIGN_KEY.name());
        }
    }

    @Override
    public CreditRequestModel findByCreditRequestSameDay(Long id, ClientModel client, String status, LocalDate dateElaboration) {
        log.info("Start findByCreditRequestSameDay [{}]", id);
        ClientEntity clientEntity = clientEntityMapper.toEntity(client);
        CreditRequestEntity creditRequestEntity = creditRequestJpaRepository.findByIdNotAndClientAndStatusAndDateElaboration(id, clientEntity, status, dateElaboration);
        return creditRequestEntityMapper.toDomain(creditRequestEntity);
    }

    @Override
    public CreditRequestModel findByCreditRequestReservedCar(Long id, CarModel car, String status) {
        log.info("Start findByCreditRequestReservedCar [{}]", id);
        CarEntity carEntity = carEntityMapper.toEntity(car);
        CreditRequestEntity creditRequestEntity = creditRequestJpaRepository.findByIdNotAndCarAndStatus(id, carEntity, status);
        return creditRequestEntityMapper.toDomain(creditRequestEntity);
    }
}
