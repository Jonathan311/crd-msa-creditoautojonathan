package com.pichincha.creditoautojonathan.infrastructure.output.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Builder
@Data
@Entity
@Table(name = "credit_request")
@NoArgsConstructor
@AllArgsConstructor
public class CreditRequestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="date_elaboration")
    private LocalDate dateElaboration;

    @ManyToOne
    @JoinColumn(name = "id_client")
    private ClientEntity client;

    @ManyToOne
    @JoinColumn(name = "id_yard")
    private YardEntity yard;

    @ManyToOne
    @JoinColumn(name = "id_executive")
    private ExecutiveEntity executive;

    @ManyToOne
    @JoinColumn(name = "id_car")
    private CarEntity car;

    @Column(name="months_term")
    private Integer monthsTerm;

    private Integer dues;

    private BigDecimal entry;

    private String status;

    private String observation;
}
