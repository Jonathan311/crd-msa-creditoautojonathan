package com.pichincha.creditoautojonathan.infrastructure.output.repository.mapper;

import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.CarEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CarEntityMapper {
    List<CarModel> toDomain(List<CarEntity> carEntities);
    CarModel toDomain(CarEntity carEntity);
    List<CarEntity> toEntity(List<CarModel> carModels);

    CarEntity toEntity(CarModel carModel);
}
