package com.pichincha.creditoautojonathan.infrastructure.output.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Builder
@Data
@Entity
@Table(name = "yard")
@NoArgsConstructor
@AllArgsConstructor
public class YardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String address;

    private String phone;

    @Column(name="point_sale_number")
    private Integer pointSaleNumber;
}
