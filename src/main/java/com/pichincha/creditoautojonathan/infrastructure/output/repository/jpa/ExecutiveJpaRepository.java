package com.pichincha.creditoautojonathan.infrastructure.output.repository.jpa;

import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.ExecutiveEntity;
import com.pichincha.creditoautojonathan.infrastructure.output.repository.entity.YardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExecutiveJpaRepository extends JpaRepository<ExecutiveEntity, Long> {
    List<ExecutiveEntity> findAllByYard(YardEntity yard);
}
