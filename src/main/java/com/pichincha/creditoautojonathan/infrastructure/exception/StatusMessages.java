package com.pichincha.creditoautojonathan.infrastructure.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusMessages {

    HTTP_BAD_REQUEST(400, 400, "Invalid input parameters");

    private final int statusCode;
    private final int code;
    private final String descriptionCode;
}
