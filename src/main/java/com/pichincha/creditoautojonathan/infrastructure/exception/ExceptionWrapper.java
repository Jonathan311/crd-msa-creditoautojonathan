package com.pichincha.creditoautojonathan.infrastructure.exception;

import com.pichincha.creditoautojonathan.commons.CustomResponseCode;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.Fails;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.MappingResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public final class ExceptionWrapper extends ResponseEntityExceptionHandler {

    private static final String ERROR_MESSAGE_LOG = "Se ha presentado el siguiente error: {} code: {} message: {}";

    @Autowired
    private CustomResponseCode customResponseCode;

    @Autowired
    private MappingResponse mappingResponse;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
        log.error(ERROR_MESSAGE_LOG, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), ex.toString());
        return customResponseCode.buildResponseEntity(HttpStatus.BAD_REQUEST, String.valueOf(StatusMessages.HTTP_BAD_REQUEST.getCode()), ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
        String message = String.format("%s:[%s]", StatusMessages.HTTP_BAD_REQUEST.getDescriptionCode(), UtilsHelper.conversionExceptionToString(ex));
        log.error(ERROR_MESSAGE_LOG, HttpStatus.BAD_REQUEST, StatusMessages.HTTP_BAD_REQUEST.getCode(), message);
        return customResponseCode.buildResponseEntity(HttpStatus.BAD_REQUEST, String.valueOf(StatusMessages.HTTP_BAD_REQUEST.getCode()), message);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        String message = String.format("%s:[%s]", StatusMessages.HTTP_BAD_REQUEST.getDescriptionCode(),ex.getMessage());
        log.error(ERROR_MESSAGE_LOG, HttpStatus.BAD_REQUEST, StatusMessages.HTTP_BAD_REQUEST.getCode(), message);
        return customResponseCode.buildResponseEntity(HttpStatus.BAD_REQUEST, String.valueOf(StatusMessages.HTTP_BAD_REQUEST.getCode()), message);
    }

    @ExceptionHandler(ManagerException.class)
    protected ResponseEntity<Object> handleManagerException(ManagerException ex) {
        String code = ex.getCode();
        String message = ex.getMessage();

        if(!Objects.isNull(ex.getIdentity())) {
            Fails fail = mappingResponse.searchFail(ex.getIdentity());
            code = fail.getCode();
            message = fail.getMessage();
        }

        log.error(ERROR_MESSAGE_LOG, HttpStatus.BAD_REQUEST, code, message);
        return customResponseCode.buildResponseEntity(HttpStatus.BAD_REQUEST, code, message);
    }
}