package com.pichincha.creditoautojonathan.infrastructure.exception;

import lombok.Getter;

@Getter
public class ManagerException extends RuntimeException {

    private String identity;
    private String code;
    private String message;

    public ManagerException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public ManagerException(String identity) {
        this.identity = identity;
    }
}
