package com.pichincha.creditoautojonathan.infrastructure.exception.mapping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Fails {

    private String identity;
    private String message;
    private String code;
}
