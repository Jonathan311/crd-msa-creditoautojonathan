package com.pichincha.creditoautojonathan.infrastructure.exception.mapping;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.List;

@Data
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "status-codes")
public class MappingResponse {

    private static final String DEFAULT_CODE = "503";
    private static final String DEFAULT_MESSAGE = "Internal server error";

    private List<Fails> fails;

    /**
     * Busqueda de codigos erroneos en el .YML a través del codigo, identificador o el codigo por defecto
     * @return
     */
    public Fails searchFail(String identity)
    {
        return this.fails.stream()
                .filter(find -> find.getIdentity().equals(identity))
                .findAny()
                .orElse(Fails.builder().code(DEFAULT_CODE).message(DEFAULT_MESSAGE).build());
    }
}
