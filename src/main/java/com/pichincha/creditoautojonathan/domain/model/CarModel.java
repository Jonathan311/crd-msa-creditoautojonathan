package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarModel extends IModel {
    private Long id;
    private String plate;
    private String model;
    private String numberChassis;
    private String type;
    private String cylindrical;
    private String appraisal;
    private String status;
    private BrandModel brand;
    @Override
    public String protectedToString() {
        return toJson("");
    }
}
