package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExecutiveModel extends IModel {
    private Long id;
    private String identification;
    private String name;
    private String surname;
    private String address;
    private String conventionalTelephone;
    private String cellPhone;
    private YardModel yard;
    private Long yardId;
    private Integer age;

    public String uniqueKey() {
        return identification + name + surname;
    }

    @Override
    public String protectedToString() {
        return toJson("identification");
    }
}
