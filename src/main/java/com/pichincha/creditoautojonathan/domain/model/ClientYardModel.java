package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientYardModel extends IModel {
    private Long id;
    private Date dateAssignment;
    private ClientModel client;
    private YardModel yard;
    public String protectedToString() {
        return toJson("");
    }
}
