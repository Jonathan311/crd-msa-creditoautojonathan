package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class YardModel extends IModel {
    private Long id;
    private String name;
    private String address;
    private String phone;
    private Integer pointSaleNumber;
    @Override
    public String protectedToString() {
        return toJson("");
    }
}
