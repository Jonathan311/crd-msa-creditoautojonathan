package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClientModel extends IModel {
    private Long id;
    private String identification;
    private String name;
    private Integer age;
    private Date birthdate;
    private String surname;
    private String address;
    private String phone;
    private String civilStatus;
    private String spouseIdentification;
    private String spouseName;
    private Boolean creditSubject;

    public String uniqueKey() {
        return identification + name + surname;
    }
    @Override
    public String protectedToString() {
        return toJson("identification");
    }
}
