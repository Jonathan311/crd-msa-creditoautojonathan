package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BrandModel extends IModel {
    private Long id;
    private String name;

    public String uniqueKey() {
        return name;
    }
    @Override
    public String protectedToString() {
        return toJson("name");
    }
}
