package com.pichincha.creditoautojonathan.domain.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.pichincha.creditoautojonathan.commons.IModel;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreditRequestModel extends IModel {
    private Long id;
    private LocalDate dateElaboration;
    private ClientModel client;
    private YardModel yard;
    private ExecutiveModel executive;
    private CarModel car;
    private Integer monthsTerm;
    private Integer dues;
    private BigDecimal entry;
    private String status;
    private String observation;
    @Override
    public String protectedToString() {
        return toJson("");
    }
}
