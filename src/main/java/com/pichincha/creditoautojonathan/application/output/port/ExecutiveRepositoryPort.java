package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.domain.model.YardModel;

import java.util.List;

public interface ExecutiveRepositoryPort {
    void loadData(List<ExecutiveModel> executiveModels);
    List<ExecutiveModel> getAll();
    List<ExecutiveModel> getAllYard(YardModel yardModel);
    ExecutiveModel get(Integer id);
    ExecutiveModel create(ExecutiveModel executiveModel);
    ExecutiveModel update(ExecutiveModel executiveModel);
    void delete(ExecutiveModel executiveModel);
}
