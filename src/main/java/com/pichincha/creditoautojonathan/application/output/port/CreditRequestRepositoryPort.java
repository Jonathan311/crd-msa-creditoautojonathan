package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.domain.model.CreditRequestModel;

import java.time.LocalDate;
import java.util.List;

public interface CreditRequestRepositoryPort {
    List<CreditRequestModel> getAll();

    CreditRequestModel get(Integer id);

    CreditRequestModel create(CreditRequestModel creditRequestModel);

    CreditRequestModel update(CreditRequestModel creditRequestModel);

    void delete(CreditRequestModel creditRequestModel);

    CreditRequestModel findByCreditRequestSameDay(Long id, ClientModel client, String status, LocalDate dateElaboration);

    CreditRequestModel findByCreditRequestReservedCar(Long id, CarModel car, String status);
}
