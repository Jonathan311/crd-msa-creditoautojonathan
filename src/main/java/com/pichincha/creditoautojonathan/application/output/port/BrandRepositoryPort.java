package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.BrandModel;

import java.util.List;

public interface BrandRepositoryPort {
    void loadData(List<BrandModel> brandModels);
    List<BrandModel> getAll();
    BrandModel get(Integer id);
}
