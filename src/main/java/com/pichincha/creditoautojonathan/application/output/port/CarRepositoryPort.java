package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.CarModel;

import java.util.List;

public interface CarRepositoryPort {
    List<CarModel> getAll();
    CarModel get(Integer id);
    CarModel create(CarModel carModel);
    CarModel update(CarModel carModel);
    void delete(CarModel carModel);
    List<CarModel> getPlate(String plate, Long id);

    List<CarModel> getBrandOrModel(Long id, String model);
}
