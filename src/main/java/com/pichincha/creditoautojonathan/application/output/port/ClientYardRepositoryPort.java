package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;

import java.util.List;

public interface ClientYardRepositoryPort {
    List<ClientYardModel> getAll();
    ClientYardModel get(Integer id);
    ClientYardModel create(ClientYardModel clientYardModel);
    ClientYardModel update(ClientYardModel clientYardModel);
    void delete(ClientYardModel clientYardModel);
}
