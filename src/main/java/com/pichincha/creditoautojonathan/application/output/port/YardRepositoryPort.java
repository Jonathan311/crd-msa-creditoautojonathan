package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.YardModel;

import java.util.List;

public interface YardRepositoryPort {
    List<YardModel> getAll();
    YardModel get(Integer id);
    YardModel create(YardModel yardModel);
    YardModel update(YardModel yardModel);
    void delete(YardModel yardModel);
}
