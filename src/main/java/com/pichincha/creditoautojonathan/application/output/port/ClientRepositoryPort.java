package com.pichincha.creditoautojonathan.application.output.port;

import com.pichincha.creditoautojonathan.domain.model.ClientModel;

import java.util.List;

public interface ClientRepositoryPort {
    void loadData(List<ClientModel> clientModels);
    List<ClientModel> getAll();
    ClientModel get(Integer id);
    ClientModel create(ClientModel clientModel);
    ClientModel update(ClientModel clientModel);
    void delete(ClientModel clientModel);
}
