package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.BrandModel;

import java.util.List;

public interface BrandService {
    void loadData();
    List<BrandModel> getAll();
}
