package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;

import java.util.List;

public interface ClientYardService {
    List<ClientYardModel> getAll();
    ClientYardModel get(Integer id);
    ClientYardModel create(ClientYardModel clientYardModel);
    ClientYardModel update(ClientYardModel clientYardModel);
    void delete(Integer id);
}
