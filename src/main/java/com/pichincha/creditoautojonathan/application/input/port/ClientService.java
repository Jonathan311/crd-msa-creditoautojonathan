package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.ClientModel;

import java.util.List;

public interface ClientService {
    void loadData();
    List<ClientModel> getAll();
    ClientModel get(Integer id);
    ClientModel create(ClientModel clientModel);
    ClientModel update(ClientModel clientModel);
    void delete(Integer id);
}
