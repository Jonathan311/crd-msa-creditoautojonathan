package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.YardModel;

import java.util.List;

public interface YardService {
    List<YardModel> getAll();
    YardModel get(Integer id);
    YardModel create(YardModel yardModel);
    YardModel update(YardModel yardModel);
    void delete(Integer id);
}
