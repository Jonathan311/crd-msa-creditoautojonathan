package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;

import java.util.List;

public interface ExecutiveService {
    void loadData();
    List<ExecutiveModel> getAll();
    List<ExecutiveModel> getAllYard(Integer yardId);
    ExecutiveModel get(Integer id);
    ExecutiveModel create(ExecutiveModel executiveModel);
    ExecutiveModel update(ExecutiveModel executiveModel);
    void delete(Integer id);
}
