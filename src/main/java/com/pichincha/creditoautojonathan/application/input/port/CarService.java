package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.CarModel;

import java.util.List;

public interface CarService {
    List<CarModel> getAll();
    CarModel get(Integer id);
    CarModel create(CarModel carModel);
    CarModel update(CarModel carModel);
    void delete(Integer id);

    List<CarModel> getBrandOrModel(Long id, String model);
}
