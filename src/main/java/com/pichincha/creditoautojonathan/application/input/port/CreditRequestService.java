package com.pichincha.creditoautojonathan.application.input.port;

import com.pichincha.creditoautojonathan.domain.model.CreditRequestModel;

import java.util.List;

public interface CreditRequestService {
    List<CreditRequestModel> getAll();
    CreditRequestModel get(Integer id);
    CreditRequestModel create(CreditRequestModel creditRequestModel);
    CreditRequestModel update(CreditRequestModel creditRequestModel);
    CreditRequestModel patch(CreditRequestModel creditRequestModel);
    void delete(Integer id);
}
