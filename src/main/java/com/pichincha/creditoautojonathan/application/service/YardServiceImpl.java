package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.YardService;
import com.pichincha.creditoautojonathan.application.output.port.YardRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class YardServiceImpl implements YardService {

    private final YardRepositoryPort yardRepositoryPort;

    @Override
    public List<YardModel> getAll() {
        log.info("Start Domain Service getAllYard");
        return yardRepositoryPort.getAll();
    }

    @Override
    public YardModel get(Integer id) {
        log.info("Start Domain Service getYard:[{}]", id);
        return yardRepositoryPort.get(id);
    }

    @Override
    public YardModel create(YardModel yardModel) {
        log.info("Start Domain Service createYard:[{}]", yardModel.protectedToString());
        yardModel = yardRepositoryPort.create(yardModel);
        log.info("End Domain Service createYard");
        return yardModel;
    }

    @Override
    public YardModel update(YardModel yardModel) {
        log.info("Start Domain Service updateYard:[{}]", yardModel.protectedToString());
        YardModel yardFound = yardRepositoryPort.get(Math.toIntExact(yardModel.getId()));
        yardFound.setName(yardModel.getName());
        yardFound.setAddress(yardModel.getAddress());
        yardFound.setPhone(yardModel.getPhone());
        yardFound.setPointSaleNumber(yardModel.getPointSaleNumber());
        yardModel = yardRepositoryPort.update(yardFound);

        log.info("End Domain Service updateYard");

        return yardModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteYard:[{}]", id);
        final YardModel yardFound = yardRepositoryPort.get(id);
        yardRepositoryPort.delete(yardFound);
        log.info("End Domain Service deleteYard");
    }
}
