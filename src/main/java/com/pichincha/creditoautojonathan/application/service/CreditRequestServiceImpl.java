package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.CreditRequestService;
import com.pichincha.creditoautojonathan.application.output.port.*;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import com.pichincha.creditoautojonathan.domain.model.*;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;


@Slf4j
@RequiredArgsConstructor
@Service
public class CreditRequestServiceImpl implements CreditRequestService {

    private final CreditRequestRepositoryPort creditRequestRepositoryPort;

    private final YardRepositoryPort yardRepositoryPort;

    private final ClientRepositoryPort clientRepositoryPort;

    private final ExecutiveRepositoryPort executiveRepositoryPort;

    private final CarRepositoryPort carRepositoryPort;

    @Override
    public List<CreditRequestModel> getAll() {
        log.info("Start Domain Service getAllCreditRequest");
        return creditRequestRepositoryPort.getAll();
    }

    @Override
    public CreditRequestModel get(Integer id) {
        log.info("Start Domain Service getCreditRequest:[{}]", id);
        return creditRequestRepositoryPort.get(id);
    }

    @Override
    public CreditRequestModel create(CreditRequestModel creditRequestModel) {
        log.info("Start Domain Service createCreditRequest:[{}]", creditRequestModel.protectedToString());

        YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(creditRequestModel.getYard().getId()));
        creditRequestModel.setYard(yardModel);

        ClientModel clientModel = clientRepositoryPort.get(Math.toIntExact(creditRequestModel.getClient().getId()));
        creditRequestModel.setClient(clientModel);

        ExecutiveModel executiveModel = executiveRepositoryPort.get(Math.toIntExact(creditRequestModel.getExecutive().getId()));
        creditRequestModel.setExecutive(executiveModel);

        CarModel carModel = carRepositoryPort.get(Math.toIntExact(creditRequestModel.getCar().getId()));
        creditRequestModel.setCar(carModel);

        validateCreditRequestSameDay(creditRequestModel, 0L);

        validateCreditRequestReservedCar(creditRequestModel, 0L);

        creditRequestModel = creditRequestRepositoryPort.create(creditRequestModel);
        log.info("End Domain Service createCreditRequest");
        return creditRequestModel;
    }

    @Override
    public CreditRequestModel update(CreditRequestModel creditRequestModel) {
        log.info("Start Domain Service updateCreditRequest:[{}]", creditRequestModel.protectedToString());

        CreditRequestModel creditRequestFound = creditRequestRepositoryPort.get(Math.toIntExact(creditRequestModel.getId()));

        YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(creditRequestModel.getYard().getId()));
        creditRequestFound.setYard(yardModel);

        ClientModel clientModel = clientRepositoryPort.get(Math.toIntExact(creditRequestModel.getClient().getId()));
        creditRequestFound.setClient(clientModel);

        ExecutiveModel executiveModel = executiveRepositoryPort.get(Math.toIntExact(creditRequestModel.getExecutive().getId()));
        creditRequestFound.setExecutive(executiveModel);

        CarModel carModel = carRepositoryPort.get(Math.toIntExact(creditRequestModel.getCar().getId()));
        creditRequestFound.setCar(carModel);

        validateCreditRequestSameDay(creditRequestModel, creditRequestModel.getId());

        validateCreditRequestReservedCar(creditRequestModel, creditRequestModel.getId());

        creditRequestModel = creditRequestRepositoryPort.update(creditRequestFound);

        log.info("End Domain Service updateCreditRequest");

        return creditRequestModel;
    }

    @Override
    public CreditRequestModel patch(CreditRequestModel creditRequestModel) {
        log.info("Start Domain Service patchCreditRequest:[{}]", creditRequestModel.protectedToString());
        CreditRequestModel creditRequestFound = creditRequestRepositoryPort.get(Math.toIntExact(creditRequestModel.getId()));

        creditRequestFound.setStatus(creditRequestModel.getStatus());

        if (!Objects.isNull(creditRequestModel.getObservation())) {
            creditRequestFound.setObservation(creditRequestModel.getObservation());
        }

        creditRequestModel = creditRequestRepositoryPort.update(creditRequestFound);

        log.info("End Domain Service patchCreditRequest");

        return creditRequestModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteCreditRequest:[{}]", id);
        final CreditRequestModel creditRequestFound = creditRequestRepositoryPort.get(id);
        creditRequestRepositoryPort.delete(creditRequestFound);
        log.info("End Domain Service deleteCreditRequest");
    }

    private void validateCreditRequestSameDay(CreditRequestModel creditRequestModel, Long id) {
        CreditRequestModel creditRequestSameDay = creditRequestRepositoryPort.findByCreditRequestSameDay(id, creditRequestModel.getClient(), Constants.STATUS_REGISTRY_CREDIT_REQUEST, creditRequestModel.getDateElaboration());
        if (!Objects.isNull(creditRequestSameDay))
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_CREDIT_SAME_DAY.name());

    }

    private void validateCreditRequestReservedCar(CreditRequestModel creditRequestModel, Long id) {
        CreditRequestModel creditRequestReservedCar = creditRequestRepositoryPort.findByCreditRequestReservedCar(id, creditRequestModel.getCar(), Constants.STATUS_REGISTRY_CREDIT_REQUEST);
        if (!Objects.isNull(creditRequestReservedCar))
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_CREDIT_CAR_RESERVED.name());

    }
}
