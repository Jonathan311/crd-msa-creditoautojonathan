package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.ExecutiveService;
import com.pichincha.creditoautojonathan.application.output.port.ExecutiveRepositoryPort;
import com.pichincha.creditoautojonathan.application.output.port.YardRepositoryPort;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.pichincha.creditoautojonathan.commons.util.ConvertCsvModel.csvToModelExecutive;
import static com.pichincha.creditoautojonathan.commons.util.RemoveDuplicate.getUniqueExecutiveModels;

@Slf4j
@RequiredArgsConstructor
@Service
public class ExecutiveServiceImpl implements ExecutiveService {

    private final ExecutiveRepositoryPort executiveRepositoryPort;

    private final YardRepositoryPort yardRepositoryPort;

    private final PropertiesFilesCsv propertiesFilesCsv;

    @Override
    public void loadData() {
        List<ExecutiveModel> executiveModels = executiveRepositoryPort.getAll();
        try (BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(propertiesFilesCsv.getExecutive())))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                ExecutiveModel executiveModel = csvToModelExecutive(values);

                YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(executiveModel.getYardId()));
                executiveModel.setYard(yardModel);

                executiveModels.add(executiveModel);
            }
        } catch (Exception e) {
            log.error("Don't read file csv EXECUTIVE");
        }
        Collection<ExecutiveModel> executiveModelCollection = getUniqueExecutiveModels(executiveModels);
        executiveRepositoryPort.loadData(new ArrayList(executiveModelCollection));
    }

    @Override
    public List<ExecutiveModel> getAll() {
        log.info("Start Domain Service getAllExecutive");
        return executiveRepositoryPort.getAll();
    }

    @Override
    public List<ExecutiveModel> getAllYard(Integer yardId) {
        log.info("Start Domain Service getAllExecutiveByYard");
        YardModel yardModel = yardRepositoryPort.get(yardId);
        return executiveRepositoryPort.getAllYard(yardModel);
    }

    @Override
    public ExecutiveModel get(Integer id) {
        log.info("Start Domain Service getExecutive:[{}]", id);
        return executiveRepositoryPort.get(id);
    }

    @Override
    public ExecutiveModel create(ExecutiveModel executiveModel) {
        log.info("Start Domain Service createExecutive:[{}]", executiveModel.protectedToString());
        YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(executiveModel.getYard().getId()));
        executiveModel.setYard(yardModel);
        executiveModel = executiveRepositoryPort.create(executiveModel);
        log.info("End Domain Service createExecutive");
        return executiveModel;
    }

    @Override
    public ExecutiveModel update(ExecutiveModel executiveModel) {
        log.info("Start Domain Service updateExecutive:[{}]", executiveModel.protectedToString());
        ExecutiveModel executiveFound = executiveRepositoryPort.get(Math.toIntExact(executiveModel.getId()));

        YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(executiveModel.getYard().getId()));
        executiveFound.setYard(yardModel);

        executiveFound.setName(executiveModel.getName());
        executiveFound.setSurname(executiveModel.getSurname());
        executiveFound.setAddress(executiveModel.getAddress());
        executiveFound.setIdentification(executiveModel.getIdentification());
        executiveFound.setAge(executiveModel.getAge());
        executiveFound.setCellPhone(executiveModel.getCellPhone());
        executiveFound.setConventionalTelephone(executiveModel.getConventionalTelephone());
        executiveModel = executiveRepositoryPort.update(executiveFound);

        log.info("End Domain Service updateExecutive");

        return executiveModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteExecutive:[{}]", id);
        final ExecutiveModel executiveFound = executiveRepositoryPort.get(id);
        executiveRepositoryPort.delete(executiveFound);
        log.info("End Domain Service deleteExecutive");
    }
}
