package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.ClientService;
import com.pichincha.creditoautojonathan.application.output.port.ClientRepositoryPort;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.pichincha.creditoautojonathan.commons.util.ConvertCsvModel.csvToModelClient;
import static com.pichincha.creditoautojonathan.commons.util.RemoveDuplicate.getUniqueClientModels;

@Slf4j
@RequiredArgsConstructor
@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepositoryPort clientRepositoryPort;

    private final PropertiesFilesCsv propertiesFilesCsv;

    @Override
    public void loadData() {
        List<ClientModel> clientModels = clientRepositoryPort.getAll();
        try (BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(propertiesFilesCsv.getClient())))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                ClientModel clientModel = csvToModelClient(values);
                clientModels.add(clientModel);
            }
        } catch (Exception e) {
            log.error("Don't read file csv CLIENT");
        }
        Collection<ClientModel> clientModelCollection = getUniqueClientModels(clientModels);
        clientRepositoryPort.loadData(new ArrayList(clientModelCollection));
    }

    @Override
    public List<ClientModel> getAll() {
        log.info("Start Domain Service getAllClient");
        return clientRepositoryPort.getAll();
    }

    @Override
    public ClientModel get(Integer id) {
        log.info("Start Domain Service getClient:[{}]", id);
        return clientRepositoryPort.get(id);
    }

    @Override
    public ClientModel create(ClientModel clientModel) {
        log.info("Start Domain Service createClient:[{}]", clientModel.protectedToString());
        clientModel = clientRepositoryPort.create(clientModel);
        log.info("End Domain Service createClient");
        return clientModel;
    }

    @Override
    public ClientModel update(ClientModel clientModel) {
        log.info("Start Domain Service updateClient:[{}]", clientModel.protectedToString());
        ClientModel clientFound = clientRepositoryPort.get(Math.toIntExact(clientModel.getId()));
        clientFound.setIdentification(clientModel.getIdentification());
        clientFound.setName(clientModel.getName());
        clientFound.setAge(clientModel.getAge());
        clientFound.setBirthdate(clientModel.getBirthdate());
        clientFound.setAddress(clientModel.getAddress());
        clientFound.setPhone(clientModel.getPhone());
        clientFound.setSurname(clientModel.getSurname());
        clientFound.setCivilStatus(clientModel.getCivilStatus());
        clientFound.setSpouseIdentification(clientModel.getSpouseIdentification());
        clientFound.setSpouseName(clientModel.getSpouseName());
        clientFound.setCreditSubject(clientModel.getCreditSubject());
        clientModel = clientRepositoryPort.update(clientFound);

        log.info("End Domain Service updateClient");

        return clientModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteClient:[{}]", id);
        final ClientModel clientFound = clientRepositoryPort.get(id);
        clientRepositoryPort.delete(clientFound);
        log.info("End Domain Service deleteClient");
    }
}
