package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.BrandService;
import com.pichincha.creditoautojonathan.application.output.port.BrandRepositoryPort;
import com.pichincha.creditoautojonathan.commons.properties.PropertiesFilesCsv;
import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.pichincha.creditoautojonathan.commons.util.RemoveDuplicate.getUniqueBrandModels;

@Slf4j
@RequiredArgsConstructor
@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepositoryPort brandRepositoryPort;

    private final PropertiesFilesCsv propertiesFilesCsv;

    @Override
    public void loadData() {
        List<BrandModel> brandModels = brandRepositoryPort.getAll();
        try (BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile(propertiesFilesCsv.getBrand())))) {
            String line;
            while ((line = br.readLine()) != null) {
                BrandModel brandModel = BrandModel.builder().name(line).build();
                brandModels.add(brandModel);
            }
        } catch (Exception e) {
            log.error("Don't read file csv BRAND");
        }
        Collection<BrandModel> brandModelCollection = getUniqueBrandModels(brandModels);
        brandRepositoryPort.loadData(new ArrayList(brandModelCollection));
    }

    @Override
    public List<BrandModel> getAll() {
        log.info("Start Domain Service getAllBrand");
        return brandRepositoryPort.getAll();
    }
}
