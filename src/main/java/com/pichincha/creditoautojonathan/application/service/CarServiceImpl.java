package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.CarService;
import com.pichincha.creditoautojonathan.application.output.port.BrandRepositoryPort;
import com.pichincha.creditoautojonathan.application.output.port.CarRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import com.pichincha.creditoautojonathan.domain.model.CarModel;
import com.pichincha.creditoautojonathan.infrastructure.exception.ManagerException;
import com.pichincha.creditoautojonathan.infrastructure.exception.mapping.TypeValidationEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class CarServiceImpl implements CarService {

    private final CarRepositoryPort carRepositoryPort;

    private final BrandRepositoryPort brandRepositoryPort;

    @Override
    public List<CarModel> getAll() {
        log.info("Start Domain Service getAllCar");
        return carRepositoryPort.getAll();
    }

    @Override
    public CarModel get(Integer id) {
        log.info("Start Domain Service getCar:[{}]", id);
        return carRepositoryPort.get(id);
    }

    @Override
    public CarModel create(CarModel carModel) {
        log.info("Start Domain Service createCar:[{}]", carModel.protectedToString());

        validatePlate(carModel.getPlate(), 0L);

        BrandModel brandModel = brandRepositoryPort.get(Math.toIntExact(carModel.getBrand().getId()));
        carModel.setBrand(brandModel);
        carModel = carRepositoryPort.create(carModel);
        log.info("End Domain Service createCar");
        return carModel;
    }

    @Override
    public CarModel update(CarModel carModel) {
        log.info("Start Domain Service updateCar:[{}]", carModel.protectedToString());

        CarModel carFound = carRepositoryPort.get(Math.toIntExact(carModel.getId()));

        validatePlate(carModel.getPlate(), carFound.getId());

        BrandModel brandModel = brandRepositoryPort.get(Math.toIntExact(carModel.getBrand().getId()));
        carFound.setBrand(brandModel);

        carFound.setPlate(carModel.getPlate());
        carFound.setModel(carModel.getModel());
        carFound.setNumberChassis(carModel.getNumberChassis());
        carFound.setType(carModel.getType());
        carFound.setCylindrical(carModel.getCylindrical());
        carFound.setAppraisal(carModel.getAppraisal());
        carFound.setStatus(carModel.getStatus());
        carModel = carRepositoryPort.update(carFound);

        log.info("End Domain Service updateCar");

        return carModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteCar:[{}]", id);
        final CarModel carFound = carRepositoryPort.get(id);
        carRepositoryPort.delete(carFound);
        log.info("End Domain Service deleteCar");
    }

    @Override
    public List<CarModel> getBrandOrModel(Long id, String model) {
        log.info("Start Domain Service getBrandOrModel:[{}]", model);
        return carRepositoryPort.getBrandOrModel(id, model);
    }

    private void validatePlate(String plate, Long id) {
        List<CarModel> carModels = carRepositoryPort.getPlate(plate, id);
        if (!carModels.isEmpty()) {
            throw new ManagerException(TypeValidationEnum.HTTP_ERR_DATA_CAR_PLATE_VIOLATION_DUPLICATE.name());
        }
    }
}
