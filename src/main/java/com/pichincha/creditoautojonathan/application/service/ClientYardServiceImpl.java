package com.pichincha.creditoautojonathan.application.service;

import com.pichincha.creditoautojonathan.application.input.port.ClientYardService;
import com.pichincha.creditoautojonathan.application.output.port.ClientRepositoryPort;
import com.pichincha.creditoautojonathan.application.output.port.ClientYardRepositoryPort;
import com.pichincha.creditoautojonathan.application.output.port.YardRepositoryPort;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.domain.model.ClientYardModel;
import com.pichincha.creditoautojonathan.domain.model.YardModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class ClientYardServiceImpl implements ClientYardService {

    private final ClientYardRepositoryPort clientYardRepositoryPort;

    private final YardRepositoryPort yardRepositoryPort;

    private final ClientRepositoryPort clientRepositoryPort;

    @Override
    public List<ClientYardModel> getAll() {
        log.info("Start Domain Service getAllClientYard");
        return clientYardRepositoryPort.getAll();
    }

    @Override
    public ClientYardModel get(Integer id) {
        log.info("Start Domain Service getClientYard:[{}]", id);
        return clientYardRepositoryPort.get(id);
    }

    @Override
    public ClientYardModel create(ClientYardModel clientYardModel) {
        log.info("Start Domain Service createClientYard:[{}]", clientYardModel.protectedToString());

        YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(clientYardModel.getYard().getId()));
        clientYardModel.setYard(yardModel);

        ClientModel clientModel = clientRepositoryPort.get(Math.toIntExact(clientYardModel.getClient().getId()));
        clientYardModel.setClient(clientModel);

        clientYardModel = clientYardRepositoryPort.create(clientYardModel);
        log.info("End Domain Service createClientYard");
        return clientYardModel;
    }

    @Override
    public ClientYardModel update(ClientYardModel clientYardModel) {
        log.info("Start Domain Service updateClientYard:[{}]", clientYardModel.protectedToString());
        ClientYardModel clientYardFound = clientYardRepositoryPort.get(Math.toIntExact(clientYardModel.getId()));
        clientYardFound.setDateAssignment(clientYardModel.getDateAssignment());

        YardModel yardModel = yardRepositoryPort.get(Math.toIntExact(clientYardModel.getYard().getId()));
        clientYardFound.setYard(yardModel);

        ClientModel clientModel = clientRepositoryPort.get(Math.toIntExact(clientYardModel.getClient().getId()));
        clientYardFound.setClient(clientModel);

        clientYardModel = clientYardRepositoryPort.update(clientYardFound);

        log.info("End Domain Service updateClientYard");

        return clientYardModel;
    }

    @Override
    public void delete(Integer id) {
        log.info("Start Domain Service deleteClientYard:[{}]", id);
        final ClientYardModel clientYardFound = clientYardRepositoryPort.get(id);
        clientYardRepositoryPort.delete(clientYardFound);
        log.info("End Domain Service deleteClientYard");
    }
}
