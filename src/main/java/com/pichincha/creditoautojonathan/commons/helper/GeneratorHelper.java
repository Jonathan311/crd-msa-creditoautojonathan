package com.pichincha.creditoautojonathan.commons.helper;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class GeneratorHelper {

    public static final SimpleDateFormat DATE_FORMAT_MED_CORRELATION_ID = new SimpleDateFormat("yyMMddHHmmssSSS");
    public static final int PIN_1 = 9;
    public static final int PIN_2 = 99;
    public static final int PIN_3 = 999;
    public static final int PIN_4 = 9999;
    public static final int PIN_5 = 99999;
    public static final int PIN_6 = 999999;
    public static final int PIN_7 = 9999999;
    public static final int PIN_8 = 99999999;
    public static final int PIN_9 = 999999999;
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final String IP_V4_FORMAT = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    private static final Pattern IP_V4_PATTERN = Pattern.compile(IP_V4_FORMAT);
    private static final String IP_V6_FORMAT = "^([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4}):([A-Fa-f0-9]{1,4})$";
    private static final Pattern IP_V6_PATTERN = Pattern.compile(IP_V6_FORMAT);
    private static final Random RANDOM = new Random();

    private GeneratorHelper() {
        super();
    }

    // UTILS

    private static int getPinSizeLength(int size) {
        int pinLen;
        switch (size) {
            case 1:
                pinLen = PIN_1;
                break;
            case 2:
                pinLen = PIN_2;
                break;
            case 3:
                pinLen = PIN_3;
                break;
            case 4:
                pinLen = PIN_4;
                break;
            case 5:
                pinLen = PIN_5;
                break;
            case 6:
                pinLen = PIN_6;
                break;
            case 7:
                pinLen = PIN_7;
                break;
            case 9:
                pinLen = PIN_9;
                break;
            default:
                pinLen = PIN_8;
        }

        return pinLen;
    }

    private static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        int alphaLen = ALPHA_NUMERIC_STRING.length();
        while ((count-1) != 0) {
            int character = RANDOM.nextInt() * alphaLen;
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    // SERVICES

    public static long correlationId() {
        return correlationId(DATE_FORMAT_MED_CORRELATION_ID, PIN_2);
    }

    public static long correlationId(SimpleDateFormat simpleDateFormat, int pinLen) {
        return Long.parseLong(simpleDateFormat.format(new Date()) + pin(pinLen));
    }


    // Formato: 255yyMMddHHmmssSSS99
    // 255: last octet from ip
    // yyMMddHHmmssSSS: Fecha actual
    // 99: Aleatorio
    public static String correlationId(String ipRequest) throws Exception {
        Matcher matcher = IP_V4_PATTERN.matcher(ipRequest);
        if (!matcher.matches()) {
            matcher = IP_V6_PATTERN.matcher(ipRequest);
            if (!matcher.matches()) {
                throw new Exception("Formato IP no valido");
            }
        }

        // IP en 3 digitos
        String ipLastOctet = matcher.group(matcher.groupCount());
        if (ipLastOctet == null) {
            ipLastOctet = "255";
        } else if (ipLastOctet.length() < 3) {
            ipLastOctet = StringUtils.leftPad(ipLastOctet, 3, '0');
        } else if (ipLastOctet.length() > 3) {
            ipLastOctet = ipLastOctet.substring(0, 3);
        }

        // Generar el Correlation ID
        return ipLastOctet.concat(String.valueOf(GeneratorHelper.correlationId()));

    }

    public static String pin() {
        return pin(PIN_4);
    }

    public static String pin(int pinLen) {
        int size;
        switch (pinLen) {
            case PIN_1:
                size = 1;
                break;
            case PIN_2:
                size = 2;
                break;
            case PIN_3:
                size = 3;
                break;
            case PIN_4:
                size = 4;
                break;
            case PIN_5:
                size = 5;
                break;
            case PIN_6:
                size = 6;
                break;
            case PIN_7:
                size = 7;
                break;
            case PIN_9:
                size = 9;
                break;
            default:
                size = 8;
        }
        return StringUtils.leftPad(String.valueOf(RANDOM.nextInt(pinLen)), size, "0");
    }

    public static String pin(int size, boolean isAlpha) {
        // PIN ALFANUMÉRICO
        if (isAlpha) {
            return randomAlphaNumeric(size);
        }

        // PIN NUMÉRICO
        return pin(getPinSizeLength(size));
    }
}
