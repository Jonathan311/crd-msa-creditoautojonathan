package com.pichincha.creditoautojonathan.commons.helper;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.lang3.StringUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public final class CommandHelper implements Serializable {

    private static final XmlMapper XML_MAPPER;

    static {
        XML_MAPPER = new XmlMapper();
        XML_MAPPER.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, SerializationFeature.WRAP_ROOT_VALUE);
        XML_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, Boolean.FALSE);
        XML_MAPPER.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, Boolean.TRUE);
    }

    private CommandHelper() {
        super();
    }

    public static String safelyCommand(@NotNull String xmlString) {
        if (StringUtils.isEmpty(xmlString)){ return xmlString; }

        // Remueve los saltos de linea y tabulaciones
        xmlString = xmlString.replaceAll("(\r\n|\n|\r|\t)", "");

        // Escape de caracter '&'
        xmlString = xmlString.replace("&", "&amp;");

        return xmlString;
    }
}
