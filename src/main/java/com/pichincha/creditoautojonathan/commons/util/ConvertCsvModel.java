package com.pichincha.creditoautojonathan.commons.util;

import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;

import java.sql.Timestamp;

public class ConvertCsvModel {

    private ConvertCsvModel() {
        throw new IllegalStateException("Utility class");
    }
    public static ExecutiveModel csvToModelExecutive(String[] array) {
        ExecutiveModel executiveModel = ExecutiveModel.builder()
                .identification(array[0])
                .name(array[1])
                .surname(array[2])
                .address(array[3])
                .conventionalTelephone(array[4])
                .cellPhone(array[5])
                .yardId(Long.valueOf(array[6]))
                .age(Integer.parseInt(array[7]))
                .build();
        return executiveModel;
    }

    public static ClientModel csvToModelClient(String[] array) {
        ClientModel clientModel = ClientModel.builder()
                .identification(array[0])
                .name(array[1])
                .age(Integer.valueOf(array[2]))
                .birthdate(Timestamp.valueOf(array[3]))
                .surname(array[4])
                .address(array[5])
                .phone(array[6])
                .civilStatus(array[7])
                .spouseIdentification(array[8])
                .spouseName(array[9])
                .creditSubject(Boolean.valueOf(array[10]))
                .build();
        return clientModel;
    }
}
