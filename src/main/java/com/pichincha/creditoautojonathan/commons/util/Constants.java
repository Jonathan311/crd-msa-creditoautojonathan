package com.pichincha.creditoautojonathan.commons.util;

import java.io.Serializable;

public final class Constants implements Serializable {

    private Constants() {
        super();
    }

    //Yml properties
    public static final String APP_SAFE = "${spring.application.services.safe-url}";
    public static final String SPRING_CONFIG_PREFIX_PROPERTIES_FILES_CSV = "properties.files";
    public static final String SPRING_CONFIG_PREFIX = "spring.application";

    //Yml controllers name
    public static final String BRAND_CONTROLLER_PATH = "${spring.application.services.names.brand.path}";
    public static final String YARD_CONTROLLER_PATH = "${spring.application.services.names.yard.path}";
    public static final String EXECUTIVE_CONTROLLER_PATH = "${spring.application.services.names.executive.path}";
    public static final String CAR_CONTROLLER_PATH = "${spring.application.services.names.car.path}";
    public static final String CLIENT_CONTROLLER_PATH = "${spring.application.services.names.client.path}";
    public static final String CLIENT_YARD_CONTROLLER_PATH = "${spring.application.services.names.clientYard.path}";
    public static final String CREDIT_CONTROLLER_PATH = "${spring.application.services.names.credit.path}";

    // Yml BRAND
    public static final String BRAND_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.brand.getAll}";

    // Yml YARD
    public static final String YARD_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.yard.getAll}";
    public static final String YARD_CONTROLLER_GET_PATH = "${spring.application.services.methods.yard.get}";
    public static final String YARD_CONTROLLER_POST_PATH = "${spring.application.services.methods.yard.post}";
    public static final String YARD_CONTROLLER_PUT_PATH = "${spring.application.services.methods.yard.put}";
    public static final String YARD_CONTROLLER_DELETE_PATH = "${spring.application.services.methods.yard.delete}";

    // Yml EXECUTIVE
    public static final String EXECUTIVE_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.executive.getAll}";
    public static final String EXECUTIVE_CONTROLLER_GET_ALL_BY_YARD_PATH = "${spring.application.services.methods.executive.getAllYard}";
    public static final String EXECUTIVE_CONTROLLER_GET_PATH = "${spring.application.services.methods.executive.get}";
    public static final String EXECUTIVE_CONTROLLER_POST_PATH = "${spring.application.services.methods.executive.post}";
    public static final String EXECUTIVE_CONTROLLER_PUT_PATH = "${spring.application.services.methods.executive.put}";
    public static final String EXECUTIVE_CONTROLLER_DELETE_PATH = "${spring.application.services.methods.executive.delete}";

    // Yml CAR
    public static final String CAR_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.car.getAll}";
    public static final String CAR_CONTROLLER_GET_PATH = "${spring.application.services.methods.car.get}";
    public static final String CAR_CONTROLLER_POST_PATH = "${spring.application.services.methods.car.post}";
    public static final String CAR_CONTROLLER_PUT_PATH = "${spring.application.services.methods.car.put}";
    public static final String CAR_CONTROLLER_DELETE_PATH = "${spring.application.services.methods.car.delete}";
    public static final String CAR_CONTROLLER_GET_ALL_FILTER_PATH = "${spring.application.services.methods.car.getAllFilter}";

    // Yml CLIENT
    public static final String CLIENT_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.client.getAll}";
    public static final String CLIENT_CONTROLLER_GET_PATH = "${spring.application.services.methods.client.get}";
    public static final String CLIENT_CONTROLLER_POST_PATH = "${spring.application.services.methods.client.post}";
    public static final String CLIENT_CONTROLLER_PUT_PATH = "${spring.application.services.methods.client.put}";
    public static final String CLIENT_CONTROLLER_DELETE_PATH = "${spring.application.services.methods.client.delete}";

    // Yml CLIENT_YARD
    public static final String CLIENT_YARD_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.clientYard.getAll}";
    public static final String CLIENT_YARD_CONTROLLER_GET_PATH = "${spring.application.services.methods.clientYard.get}";
    public static final String CLIENT_YARD_CONTROLLER_POST_PATH = "${spring.application.services.methods.clientYard.post}";
    public static final String CLIENT_YARD_CONTROLLER_PUT_PATH = "${spring.application.services.methods.clientYard.put}";
    public static final String CLIENT_YARD_CONTROLLER_DELETE_PATH = "${spring.application.services.methods.clientYard.delete}";

    // Yml CREDIT
    public static final String CREDIT_CONTROLLER_GET_ALL_PATH = "${spring.application.services.methods.credit.getAll}";
    public static final String CREDIT_CONTROLLER_GET_PATH = "${spring.application.services.methods.credit.get}";
    public static final String CREDIT_CONTROLLER_POST_PATH = "${spring.application.services.methods.credit.post}";
    public static final String CREDIT_CONTROLLER_PUT_PATH = "${spring.application.services.methods.credit.put}";
    public static final String CREDIT_CONTROLLER_PATCH_PATH = "${spring.application.services.methods.credit.patch}";
    public static final String CREDIT_CONTROLLER_DELETE_PATH = "${spring.application.services.methods.credit.delete}";

    //Util
    public static final String LONG_LINE = "------------------------------------------------";
    public static final String CORRELATIVE_HEADER = "correlation-id";
    public static final String COMPONENT_CORRELATIVE = "component";
    public static final String EMPTY_STRING = "";
    public static final String LBL_START = "INICIANDO TRANSACCIÓN";
    public static final String QUOTES = "\"";
    public static final String ASTERISK = "*";
    public static final String TWO_DOTS = ":";

    // Regex
    public static final String REGEX_ALL = ".";
    public static final String REGEX_REPLACE_JSON_VALUE = "\".*?:.*?\".*?\"";

    // Logs on start application
    public static final String LBL_RESPONSE_SERVICE = "[{}] RESPUESTA SERVICIO [{}]: [{}]";
    public static final String LBL_REQUEST_TYPE = "[{}] SOLICITUD SERVICIO [{}] PARÁMETROS: {}"; // Service request
    public static final String LOG_THIRD_REQUEST = "[{}] [{}] SOLICITUD: URL: [{}], SOLICITUD: [{}]"; // Third request
    public static final String LBL_RESPONSE = "[{}] [{}] RESPUESTA: [\"{}\"]"; // Third or service response
    public static final String LBL_END = "FINAL DE LA TRANSACCIÓN";
    public static final String LOG_START_PROJECT = "{} Aplicación iniciada";
    public static final String LOG_PORT_OF_PROJECT = "Puerto: {}";
    public static final String LOG_PROJECT_VERSION = "Versión: {}";
    public static final String LOG_RUN_OK = "Lanzamiento [OK]";

    public static final String[] TAGS_IGNORE = {"identification"};

    public static final String STATUS_REGISTRY_CREDIT_REQUEST = "R";
}