package com.pichincha.creditoautojonathan.commons.util;

import com.pichincha.creditoautojonathan.domain.model.BrandModel;
import com.pichincha.creditoautojonathan.domain.model.ClientModel;
import com.pichincha.creditoautojonathan.domain.model.ExecutiveModel;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RemoveDuplicate {

    private RemoveDuplicate() {
        throw new IllegalStateException("Utility class");
    }

    public static Collection<ClientModel> getUniqueClientModels(List<ClientModel> clientModels) {
        return clientModels.stream()
                .collect(Collectors.toMap(ClientModel::uniqueKey, Function.identity(), (a, b) -> a))
                .values();
    }
    public static Collection<ExecutiveModel> getUniqueExecutiveModels(List<ExecutiveModel> executiveModels) {
        return executiveModels.stream()
                .collect(Collectors.toMap(ExecutiveModel::uniqueKey, Function.identity(), (a, b) -> a))
                .values();
    }
    public static Collection<BrandModel> getUniqueBrandModels(List<BrandModel> brandModels) {
        return brandModels.stream()
                .collect(Collectors.toMap(BrandModel::uniqueKey, Function.identity(), (a, b) -> a))
                .values();
    }
}
