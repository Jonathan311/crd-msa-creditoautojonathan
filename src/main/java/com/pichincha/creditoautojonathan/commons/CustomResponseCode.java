package com.pichincha.creditoautojonathan.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Data
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
@Tag(name = "CustomError", description = "Respuesta de error estandar")
public class CustomResponseCode {

    @Schema(description = "Codigo de error")
    private String code;

    @Schema(description = "Motivo del error de la solicitud")
    private String message;

    public ResponseEntity<Object> buildResponseEntity(HttpStatus httpStatus, String code, String message) {
        this.code = code;
        this.message = message;
        return new ResponseEntity<>(this, httpStatus);
    }
}
