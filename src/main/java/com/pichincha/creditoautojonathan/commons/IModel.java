package com.pichincha.creditoautojonathan.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pichincha.creditoautojonathan.commons.helper.UtilsHelper;
import com.pichincha.creditoautojonathan.commons.util.Constants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
public abstract class IModel implements Serializable {

    /**
     * This method return string with data of object
     *
     * @return to String of object in json format
     */
    @Override
    public String toString() {
        return toJson();
    }

    /**
     * This method return string to show in logs with sensible fields protected
     *
     * @return protected to String of object in json format
     */
    public String toJson(String... protectFields) {
        try {
            return UtilsHelper.protectFields(toJsonString(), protectFields);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return Constants.EMPTY_STRING;
        }
    }

    private String toJsonString() throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(this);
    }

    /**
     * This method return string to show in logs with sensible fields protected
     *
     * @return protected to String of object in xml/json format
     */
    public abstract String protectedToString();

    @EqualsAndHashCode(callSuper = true)
    @Data
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public static class DefaultModel extends IModel implements Serializable {

        private String test;

        @Override
        public final String protectedToString() {
            return toJson();
        }
    }
}
