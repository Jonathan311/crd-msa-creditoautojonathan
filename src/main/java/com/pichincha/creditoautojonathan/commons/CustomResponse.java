package com.pichincha.creditoautojonathan.commons;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
@Tag(name = "CustomResponse", description = "Respuesta exitosa estandar")
public final class CustomResponse extends IModel {

    @Schema(description = "Codigo de respuesta")
    private String code;

    @Schema(description = "Mensaje de confirmación de la solicitud")
    private String message;

    @Schema(description = "Objeto que contiene datos relevantes")
    private Object data;

    public CustomResponse buildSuccessResponse(Object pData) {
        this.setCode(Integer.toString(HttpStatus.OK.value()));
        this.setMessage(HttpStatus.OK.toString());
        this.setData(pData);
        return this;
    }

    @Override
    public String protectedToString() {
        return null;
    }
}