package com.pichincha.creditoautojonathan.commons.properties;

import com.pichincha.creditoautojonathan.commons.util.Constants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = Constants.SPRING_CONFIG_PREFIX_PROPERTIES_FILES_CSV)
public class PropertiesFilesCsv {

    /**
     * File csv of brand
     */
    private String brand;

    /**
     * File csv of executive
     */
    private String executive;

    /**
     * File csv of client
     */
    private String client;
}
