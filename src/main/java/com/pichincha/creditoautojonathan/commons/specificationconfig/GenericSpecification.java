package com.pichincha.creditoautojonathan.commons.specificationconfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

@Slf4j
public class GenericSpecification<T> implements Specification<T> {

    private final SearchCriteria searchCriteria;

    public GenericSpecification(final SearchCriteria searchCriteria) {
        super();
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Object> arguments = searchCriteria.getArguments();
        Object arg = arguments.get(0);
        Join join;
        switch (searchCriteria.getSearchOperation()) {
            case EQUALITY:
                return criteriaBuilder.equal(root.get(searchCriteria.getKey()), arg);
            case JOIN_EQUALITY:
                join = root.join((String) arguments.get(1));
                return criteriaBuilder.equal(join.get(searchCriteria.getKey()), arg);
            default:
                break;
        }
        return null;
    }
}