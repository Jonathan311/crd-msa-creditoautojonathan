package com.pichincha.creditoautojonathan.commons.specificationconfig;

public enum SearchOperation {
    EQUALITY, JOIN_EQUALITY
}
