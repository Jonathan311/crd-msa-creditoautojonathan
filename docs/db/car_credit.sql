CREATE DATABASE car_credit;

CREATE TABLE IF NOT EXISTS brand
(
id INT GENERATED ALWAYS AS IDENTITY,
name VARCHAR(100),
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS car
(
id INT GENERATED ALWAYS AS IDENTITY,
plate VARCHAR(50) UNIQUE,
model VARCHAR(10),
number_chassis VARCHAR(255),
type VARCHAR(50),
cylindrical VARCHAR(50),
appraisal VARCHAR(50),
status VARCHAR(1),
id_brand INT,
PRIMARY KEY(id),
CONSTRAINT fk_carxbrand
    FOREIGN KEY(id_brand)
        REFERENCES brand(id)
);

CREATE TABLE IF NOT EXISTS yard
(
id INT GENERATED ALWAYS AS IDENTITY,
name VARCHAR(100),
address VARCHAR(100),
phone VARCHAR(20),
point_sale_number INT,
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS executive
(
id INT GENERATED ALWAYS AS IDENTITY,
identification VARCHAR(50),
name VARCHAR(100),
surname VARCHAR(100),
address VARCHAR(100),
conventional_telephone VARCHAR(50),
cell_phone VARCHAR(50),
id_yard INT,
age INT,
PRIMARY KEY(id),
CONSTRAINT fk_executivexyard
    FOREIGN KEY(id_yard)
        REFERENCES yard(id)
);

CREATE TABLE IF NOT EXISTS client
(
id INT GENERATED ALWAYS AS IDENTITY,
identification VARCHAR(50),
name VARCHAR(100),
age INT,
birthdate DATE,
surname VARCHAR(100),
address VARCHAR(100),
phone VARCHAR(50),
civil_status VARCHAR(10),
spouse_identification VARCHAR(50),
spouse_name VARCHAR(100),
credit_subject BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS client_yard
(
id INT GENERATED ALWAYS AS IDENTITY,
id_client INT,
id_yard INT,
date_assignment DATE,
PRIMARY KEY(id),
CONSTRAINT fk_client_yardxclient
    FOREIGN KEY(id_client)
        REFERENCES client(id),
CONSTRAINT fk_client_yardxyard
    FOREIGN KEY(id_yard)
        REFERENCES yard(id)
);

CREATE TABLE IF NOT EXISTS credit_request
(
id INT GENERATED ALWAYS AS IDENTITY,
date_elaboration DATE,
id_client INT,
id_yard INT,
id_executive INT,
id_car INT,
months_term INT,
dues INT,
entry NUMERIC(15,2),
status VARCHAR(1),
observation VARCHAR(255),
PRIMARY KEY(id),
CONSTRAINT fk_credit_requestxclient
    FOREIGN KEY(id_client)
        REFERENCES client(id),
CONSTRAINT fk_credit_requestxyard
    FOREIGN KEY(id_yard)
        REFERENCES yard(id),
CONSTRAINT fk_credit_requestxexecutive
    FOREIGN KEY(id_executive)
        REFERENCES executive(id),
CONSTRAINT fk_credit_requestxcar
    FOREIGN KEY(id_car)
        REFERENCES car(id)
);

